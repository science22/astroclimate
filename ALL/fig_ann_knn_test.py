from functions import*
from knn_all import*


data = np.append(water_merra_bdr, water_merra_trsk, axis = 0) 
data = np.append(data, water_merra_sv, axis = 0)
uu = np.concatenate([u_bdr, u_trsk, u_sv])
mask = np.concatenate([mask_bdr, mask_trsk, mask_sv])

y_transform_all = np.ma.masked_array(yscaler_all.fit_transform(data[:, None])[:,0], mask = mask)
pc_all, ica_all = pca_comp(uu, y_transform_all, 10)
u_all_transform = xscaler.fit_transform(pc_all)

X_train, X_test, Y_train, Y_test, train_indices, test_indices = train_test_split_custom(u_all_transform, y_transform_all, test_size=0.25)
knn_all = KNeighborsRegressor(n_neighbors=3)
knn_all.fit(X_train,Y_train)
y_pred_test = knn_all.predict(X_test)
x_scaler = yscaler_all.inverse_transform(Y_test[:, None])[:,0]
y_scaler = yscaler_all.inverse_transform(y_pred_test[:, None])
print('fit общий, все места', R_squared(x_scaler, y_scaler)),scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')

############################################################################################################
# KNN
############################################################################################################

test_indices_bdr = list(filter(lambda x: x < len(water_merra_bdr)+1, test_indices))
test_indices_trsk = list(filter(lambda x: len(water_merra_bdr) < x < len(water_merra_bdr)+len(water_merra_trsk)+1, test_indices))
test_indices_sv = list(filter(lambda x: x > len(water_merra_bdr)+len(water_merra_trsk), test_indices))
t_all_knn = np.concatenate([t_merra_bdr, t_merra_sv, t_merra_trsk])
test_time_trsk_knn = t_all_knn[test_indices_trsk]
test_time_bdr_knn = t_all_knn[test_indices_bdr]
test_time_sv_knn = t_all_knn[test_indices_sv]

pc_all_bdr = u_all_transform[test_indices_bdr]
y_transform_all_bdr = y_transform_all[test_indices_bdr]
pc_all_trsk = u_all_transform[test_indices_trsk]
y_transform_all_trsk = y_transform_all[test_indices_trsk]
pc_all_sv = u_all_transform[test_indices_sv]
y_transform_all_sv = y_transform_all[test_indices_sv]


y_pred_test = knn_all.predict(pc_all_trsk)
x_scaler = yscaler_all.inverse_transform(y_transform_all_trsk[:, None])[:,0]
y_scaler = yscaler_all.inverse_transform(y_pred_test[:, None])
x_scaler_all_sum_trsk, x_scaler_all_aut_trsk, x_scaler_all_win_trsk, x_scaler_all_sprin_trsk = season_data(x_scaler, test_time_trsk_knn)
y_scaler_all_sum_trsk, y_scaler_all_aut_trsk, y_scaler_all_win_trsk, y_scaler_all_sprin_trsk = season_data(y_scaler[:,0], test_time_trsk_knn)
print('fit общий, Терскол', R_squared(x_scaler, y_scaler)),scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')

y_pred_test = knn_all.predict(pc_all_bdr)
x_scaler = yscaler_all.inverse_transform(y_transform_all_bdr[:, None])[:,0]
y_scaler = yscaler_all.inverse_transform(y_pred_test[:, None])
x_scaler_all_sum_bdr, x_scaler_all_aut_bdr, x_scaler_all_win_bdr, x_scaler_all_sprin_bdr = season_data(x_scaler, test_time_bdr_knn)
y_scaler_all_sum_bdr, y_scaler_all_aut_bdr, y_scaler_all_win_bdr, y_scaler_all_sprin_bdr = season_data(y_scaler[:,0], test_time_bdr_knn)
print('fit общий, Бадары', R_squared(x_scaler, y_scaler)),scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')

y_pred_test = knn_all.predict(pc_all_sv)
x_scaler = yscaler_all.inverse_transform(y_transform_all_sv[:, None])[:,0]
y_scaler = yscaler_all.inverse_transform(y_pred_test[:, None])
x_scaler_all_sum_sv, x_scaler_all_aut_sv, x_scaler_all_win_sv, x_scaler_all_sprin_sv = season_data(x_scaler, test_time_sv_knn)
y_scaler_all_sum_sv, y_scaler_all_aut_sv, y_scaler_all_win_sv, y_scaler_all_sprin_sv = season_data(y_scaler[:,0], test_time_sv_knn)
print('fit общий, Шпицберген', R_squared(x_scaler, y_scaler)),scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')


x_scaler_all_sum = np.concatenate([x_scaler_all_sum_bdr, x_scaler_all_sum_sv, x_scaler_all_sum_trsk])
x_scaler_all_aut = np.concatenate([x_scaler_all_aut_bdr, x_scaler_all_aut_sv, x_scaler_all_aut_trsk])
x_scaler_all_win = np.concatenate([x_scaler_all_win_bdr, x_scaler_all_win_sv, x_scaler_all_win_trsk])
x_scaler_all_sprin = np.concatenate([x_scaler_all_sprin_bdr, x_scaler_all_sprin_sv, x_scaler_all_sprin_trsk])
y_scaler_all_sum = np.concatenate([y_scaler_all_sum_bdr, y_scaler_all_sum_sv, y_scaler_all_sum_trsk])
y_scaler_all_aut = np.concatenate([y_scaler_all_aut_bdr, y_scaler_all_aut_sv, y_scaler_all_aut_trsk])
y_scaler_all_win = np.concatenate([y_scaler_all_win_bdr, y_scaler_all_win_sv, y_scaler_all_win_trsk])
y_scaler_all_sprin = np.concatenate([y_scaler_all_sprin_bdr, y_scaler_all_sprin_sv, y_scaler_all_sprin_trsk])

x = [x_scaler_all_win, x_scaler_all_sprin, x_scaler_all_aut, x_scaler_all_sum]
y = [y_scaler_all_win, y_scaler_all_sprin, y_scaler_all_aut, y_scaler_all_sum]
season = ['Зима', 'Весна', 'Осень', 'Лето' ]
color = ['dodgerblue', 'lime', 'orange', 'darkgreen']
x_scaler = np.concatenate(x)
y_scaler = np.concatenate(y)

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(23, 10))
try:
    x_log = np.log(x_scaler)
    y_log = np.log(y_scaler)
    z = np.polyfit(x_log, y_log, 1)
    p = np.poly1d(z)
    xx = x_scaler[x_scaler.argsort()]
except:
    try:
        x_log = np.log(x_scaler)
        y_log = np.log(y_scaler.ravel())
        z = np.polyfit(x_log, y_log, 1)
        p = np.poly1d(z)
        xx = x_scaler[x_scaler.argsort()]
    except:
        x_log = np.log(x_scaler.ravel())
        y_log = np.log(y_scaler)
        z = np.polyfit(x_log, y_log, 1)
        p = np.poly1d(z)
        xx = y_scaler[y_scaler.argsort()]
ax1.set_xlabel('{}'.format('MERRA-2, cm'), size=30)
ax1.set_ylabel('{}'.format('Prediction, cm'), size=30)
for i in range(len(x)):
    ax1.scatter(x[i], y[i], s=4, marker='x',alpha=0.9, label=season[i], color = color[i])
ax1.plot(xx, p(xx), color='k', linestyle='-.', linewidth=2, label=('{var} = {value}'.format(var='$R^2$', value='%.2f' % R_squared(x_scaler, y_scaler))))
ax1.semilogx()
ax1.semilogy()
ax1.set_xlim(10**(-1))
ax1.set_ylim(10**(-1))
ax1.legend(fontsize=26, markerscale=5)
ax1.tick_params(axis='both', which='major', labelsize=28)

############################################################################################################
# нейросетка
############################################################################################################

data = np.append(water_merra_bdr, water_merra_trsk, axis = 0) 
data = np.append(data, water_merra_sv, axis = 0)
uu = np.concatenate([u_bdr, u_trsk, u_sv])
mask = np.concatenate([mask_bdr, mask_trsk, mask_sv])
y_transform = np.ma.masked_array(yscaler_all.fit_transform(data[:, None])[:,0], mask = mask)
pc_all, ica_all = pca_comp(uu, y_transform, 10)
t_all_ann = np.concatenate([t_merra_bdr, t_merra_trsk, t_merra_sv])

y_transform_bdr = np.ma.masked_array(yscaler_all.transform(water_merra_bdr[:, None])[:,0], mask = mask_bdr)
pc_bdr =  ica_all.transform(u_bdr)
y_transform_trsk = np.ma.masked_array(yscaler_all.transform(water_merra_trsk[:, None])[:,0], mask = mask_trsk)
pc_trsk =  ica_all.transform(u_trsk)
y_transform_sv = np.ma.masked_array(yscaler_all.transform(water_merra_sv[:, None])[:,0], mask = mask_sv)
pc_sv =  ica_all.transform(u_sv)


y_transform_ = y_transform[2:-2]
X_ = np.zeros((pc_all.shape[0]-4, 5, pc_all.shape[1]))
for i in range(5):
    X_[:,i] = pc_all[i:pc_all.shape[0]-(4)+i]
X_train, X_test, Y_train, Y_test, train_indices, test_indices = train_test_split_custom(X_, y_transform_, test_size=0.25)

i=6
ss= np.array([1, 10])
input_shape = u_new_transform.shape  #array([ 1, 10])
model = keras.Sequential() 
model.build(input_shape) 
model.add(Dense((i+1)*ss.prod(), activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
model.add(LSTM(200, activation='relu', return_sequences =True)) # рекурентная нейронная сеть с долгосрочной памятью (обрабатывает данные в виде shape = (xx, xx, xx))
model.add(Dense((i+1)*ss.prod(), activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
model.add(layers.Flatten(input_shape=(ss))) # сглаженный слой (объединяет даные в одномерный вектор)
model.add(Dense((i+1)*ss.prod()**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
model.add(Dense((i+1)*ss[1]**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
model.add(Dense(ss[1], activation='softmax'))
model.add(Dense(1)) # выходной слой
model.compile(optimizer='adam', loss='mean_absolute_error')

logdir=f"logs/fit/{i}/"
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
model.fit(X_train, Y_train, epochs=64, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])

from keras.utils import plot_model
plot_model(model, to_file='model.png', show_shapes=True)
y_pred_test = model.predict(X_test)[:,0]
y_pred_train = model.predict(X_train)[:,0]  
x_scaler_test = yscaler_all.inverse_transform(Y_test[:, None])[:,0]
y_scaler_test = yscaler_all.inverse_transform(y_pred_test[:, None])
R_squared(x_scaler_test, y_scaler_test)


test_indices_bdr = list(filter(lambda x: x < len(water_merra_bdr)-1, test_indices))
test_indices_trsk = list(filter(lambda x: len(water_merra_bdr)-2 < x < len(water_merra_bdr)+len(water_merra_trsk)-1, test_indices))
test_indices_sv = list(filter(lambda x: x > len(water_merra_bdr)+len(water_merra_trsk)-2, test_indices))
t_all_knn = np.concatenate([t_merra_bdr, t_merra_sv, t_merra_trsk])
t_all_knn = t_all_knn [2:-2]
test_time_trsk_knn = t_all_knn[test_indices_trsk]
test_time_bdr_knn = t_all_knn[test_indices_bdr]
test_time_sv_knn = t_all_knn[test_indices_sv]


pc_all_bdr = X_[test_indices_bdr]
y_transform_all_bdr = y_transform_[test_indices_bdr]
pc_all_trsk = X_[test_indices_trsk]
y_transform_all_trsk = y_transform_[test_indices_trsk]
pc_all_sv = X_[test_indices_sv]
y_transform_all_sv = y_transform_[test_indices_sv]


y_pred_test = model.predict(pc_all_trsk)
x_scaler = yscaler_all.inverse_transform(y_transform_all_trsk[:, None])[:,0]
y_scaler = yscaler_all.inverse_transform(y_pred_test)[:,0]
print(R_squared(x_scaler, y_scaler))
x_scaler_all_sum_trsk, x_scaler_all_aut_trsk, x_scaler_all_win_trsk, x_scaler_all_sprin_trsk = season_data(x_scaler, test_time_trsk_knn)
y_scaler_all_sum_trsk, y_scaler_all_aut_trsk, y_scaler_all_win_trsk, y_scaler_all_sprin_trsk = season_data(y_scaler, test_time_trsk_knn)

y_pred_test = model.predict(pc_all_bdr)
x_scaler = yscaler_all.inverse_transform(y_transform_all_bdr[:, None])[:,0]
y_scaler = yscaler_all.inverse_transform(y_pred_test)[:,0]
print(R_squared(x_scaler, y_scaler))
x_scaler_all_sum_bdr, x_scaler_all_aut_bdr, x_scaler_all_win_bdr, x_scaler_all_sprin_bdr = season_data(x_scaler, test_time_bdr_knn)
y_scaler_all_sum_bdr, y_scaler_all_aut_bdr, y_scaler_all_win_bdr, y_scaler_all_sprin_bdr = season_data(y_scaler, test_time_bdr_knn)

y_pred_test = model.predict(pc_all_sv)
x_scaler = yscaler_all.inverse_transform(y_transform_all_sv[:, None])[:,0]
y_scaler = yscaler_all.inverse_transform(y_pred_test)[:,0]
print(R_squared(x_scaler, y_scaler))
x_scaler_all_sum_sv, x_scaler_all_aut_sv, x_scaler_all_win_sv, x_scaler_all_sprin_sv = season_data(x_scaler, test_time_sv_knn)
y_scaler_all_sum_sv, y_scaler_all_aut_sv, y_scaler_all_win_sv, y_scaler_all_sprin_sv = season_data(y_scaler, test_time_sv_knn)

x_scaler_all_sum = np.concatenate([x_scaler_all_sum_bdr, x_scaler_all_sum_sv, x_scaler_all_sum_trsk])
x_scaler_all_aut = np.concatenate([x_scaler_all_aut_bdr, x_scaler_all_aut_sv, x_scaler_all_aut_trsk])
x_scaler_all_win = np.concatenate([x_scaler_all_win_bdr, x_scaler_all_win_sv, x_scaler_all_win_trsk])
x_scaler_all_sprin = np.concatenate([x_scaler_all_sprin_bdr, x_scaler_all_sprin_sv, x_scaler_all_sprin_trsk])
y_scaler_all_sum = np.concatenate([y_scaler_all_sum_bdr, y_scaler_all_sum_sv, y_scaler_all_sum_trsk])
y_scaler_all_aut = np.concatenate([y_scaler_all_aut_bdr, y_scaler_all_aut_sv, y_scaler_all_aut_trsk])
y_scaler_all_win = np.concatenate([y_scaler_all_win_bdr, y_scaler_all_win_sv, y_scaler_all_win_trsk])
y_scaler_all_sprin = np.concatenate([y_scaler_all_sprin_bdr, y_scaler_all_sprin_sv, y_scaler_all_sprin_trsk])

x = [x_scaler_all_win, x_scaler_all_sprin, x_scaler_all_aut, x_scaler_all_sum]
y = [y_scaler_all_win, y_scaler_all_sprin, y_scaler_all_aut, y_scaler_all_sum]
season = ['Зима', 'Весна', 'Осень', 'Лето' ]
color = ['dodgerblue', 'lime', 'orange', 'darkgreen']
x_scaler = np.concatenate(x)
y_scaler = np.concatenate(y)
try:
    x_log = np.log(x_scaler)
    y_log = np.log(y_scaler)
    z = np.polyfit(x_log, y_log, 1)
    p = np.poly1d(z)
    xx = x_scaler[x_scaler.argsort()]
except:
    try:
        x_log = np.log(x_scaler)
        y_log = np.log(y_scaler.ravel())
        z = np.polyfit(x_log, y_log, 1)
        p = np.poly1d(z)
        xx = x_scaler[x_scaler.argsort()]
    except:
        x_log = np.log(x_scaler.ravel())
        y_log = np.log(y_scaler)
        z = np.polyfit(x_log, y_log, 1)
        p = np.poly1d(z)
        xx = y_scaler[y_scaler.argsort()]
        
ax2.set_xlabel('{}'.format('MERRA-2, cm'), size=30)
ax2.set_ylabel('{}'.format('Prediction, cm'), size=30)
for i in range(len(x)):
    ax2.scatter(x[i], y[i], s=4, marker='x',alpha=0.9, label=season[i], color = color[i])
ax2.plot(xx, p(xx), color='k', linestyle='-.', linewidth=2, label=('{var} = {value}'.format(var='$R^2$', value='%.2f' % R_squared(x_scaler, y_scaler))))
ax2.semilogx()
ax2.semilogy()
ax2.set_xlim(10**(-1))
ax2.set_ylim(10**(-1))
ax2.legend(fontsize=26, markerscale=5)
ax2.tick_params(axis='both', which='major', labelsize=28)


plt.subplots_adjust(hspace=0.5)
plt.savefig('/scatt_ann_knn_test_data.pdf')