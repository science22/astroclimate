from knn_all import*
import pickle


path_save_data = 'data/new_data' 
with open('{}/yscaler_all.pkl'.format(path_save_data[:4]), 'rb') as file:
    yscaler_all = pickle.load(file)
with open('{}/ica_all.pkl'.format(path_save_data[:4]), 'rb') as file:
     ica_all = pickle.load(file)   
with open('{}/knn_model_all.pkl'.format(path_save_data[:4]), 'rb') as file:
    knn_all = pickle.load(file)



NEW_trsk_miap = np.load('{}/NEW_trsk_miap.npy'.format(path_save_data))
NEW_trsk_transform = np.load('{}/NEW_trsk_merra_transform.npy'.format(path_save_data))
NEW_trsk_transform = np.ma.array(NEW_trsk_transform, mask=np.full(len(NEW_trsk_transform), False))


NEW_bdr_miap = np.load('{}/NEW_bdr_miap.npy'.format(path_save_data))
NEW_transform_bdr =np.load('{}/NEW_bdr_merra_transform.npy'.format(path_save_data))
NEW_transform_bdr = np.ma.array(NEW_transform_bdr, mask=np.full(len(NEW_transform_bdr), False))

NEW_sv_miap = np.load('{}/NEW_sv_miap.npy'.format(path_save_data))
NEW_transform_sv = np.load('{}/NEW_sv_merra_transform.npy'.format(path_save_data))
NEW_transform_sv = np.ma.array(NEW_transform_sv, mask=np.full(len(NEW_transform_sv), False))

#####################

y_transform = np.ma.masked_array(yscaler.transform(water_merra_bdr[:, None])[:,0], mask=mask_bdr)
pc_bdr =  ica_all.transform(u_bdr)
u_new_transform = xscaler.fit_transform(pc_bdr)

y_transform = y_transform[2:-2]
X_ = np.zeros((pc_bdr.shape[0]-4, 5, pc_bdr.shape[1]))
for i in range(5):
    X_[:,i] = u_new_transform[i:u_new_transform.shape[0]-(4)+i]
input_shape = X_.shape

pred_test_bdr, pred_train_bdr, history_bdr, model = ann_fit(X_, y_transform, yscaler, 6, path_bdr_ann, input_shape, np.array([1, 10]))

y_pred = model.predict(X_)[:,0] 
x_scaler = yscaler.inverse_transform(y_transform[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred[:, None])
scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')

#####################

y_transform = np.ma.masked_array(yscaler.transform(water_merra_sv[:, None])[:,0], mask=mask_sv)
pc_sv =  ica_all.transform(u_sv)
u_new_transform = xscaler.fit_transform(pc_sv)

y_transform = y_transform[2:-2]
X_ = np.zeros((pc_sv.shape[0]-4, 5, pc_sv.shape[1]))
for i in range(5):
    X_[:,i] = pc_sv[i:pc_sv.shape[0]-(4)+i]
X_train, X_test, Y_train, Y_test = train_test_split(X_, y_transform, test_size=0.25)

for layer in model.layers:
    layer.trainable = False
    
logdir=f"logs/fit/{i}/"
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
model.fit(X_train, Y_train, epochs=64, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])

y_pred_test = model.predict(X_test)[:,0]
x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred_test[:, None])
scatt_R(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm', path_sv_ann)

#####################
y_transform = np.ma.masked_array(yscaler.transform(water_merra_trsk[:, None])[:,0])
y_transform.mask = mask_trsk
pc_trsk =  ica_all.transform(u_trsk)
u_new_transform = xscaler.fit_transform(pc_trsk)

y_transform = y_transform[2:-2]
X_ = np.zeros((u_new_transform.shape[0]-4, 5, u_new_transform.shape[1]))
for i in range(5):
    X_[:,i] = u_new_transform[i:u_new_transform.shape[0]-(4)+i]
X_train, X_test, Y_train, Y_test = train_test_split(X_, y_transform, test_size=0.25)

for layer in model.layers:
    layer.trainable = False

model.fit(X_train, Y_train, epochs=64, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])
y_pred_test = model.predict(X_test)[:,0]
y_pred_train = model.predict(X_train)[:,0]  
x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred_test[:, None])
scatt_R(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm', path_trsk_ann)    
    
################################################
indices = np.arange((len(y_transform)/2 - 50), (len(y_transform)/2 + 50))
indices = indices.astype(int)

trsk_valid = y_transform[indices]
X_trsk_valid = X_[indices]
y_transform  = np.delete(y_transform, indices)
X_ = np.delete(X_, indices, axis=0)
############################################################





y_transform = np.ma.masked_array(yscaler.transform(water_merra_bdr[:, None])[:,0], mask=mask_bdr)
pc_bdr =  ica_all.transform(u_bdr)
u_new_transform = xscaler.fit_transform(pc_bdr)

y_transform = y_transform[2:-2]
X_ = np.zeros((pc_bdr.shape[0]-4, 5, pc_bdr.shape[1]))
for i in range(5):
    X_[:,i] = u_new_transform[i:u_new_transform.shape[0]-(4)+i]
y_pred = model.predict(X_)[:,0] 
x_scaler = yscaler.inverse_transform(y_transform[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred[:, None])
scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')