from functions import*
from knn_all import*
import pickle
import os

path_bdr_new = 'knn_results/new_data/Badary'
path_trsk_new = 'knn_results/new_data/Terskol'
path_sv_new = 'knn_results/new_data/Svalbard'
path_save_data = 'data/new_data'
path = [path_bdr, path_trsk, path_sv, path_bdr_ann, path_trsk_ann, path_sv_ann, path_save_data]
for i, path_dir in enumerate(path):
    if not os.path.exists(path_dir):
        os.makedirs(path_dir)
        
yscaler_all = MinMaxScaler((0,1))

data = np.append(water_merra_bdr, water_merra_trsk, axis = 0) 
uu = np.append(u_bdr, u_trsk, axis = 0) 
mask = np.append(mask_bdr, mask_trsk)

data = np.append(data, water_merra_sv, axis = 0)
uu = np.append(uu, u_sv, axis = 0) 
mask = np.append(mask, mask_sv)
y_transform = np.ma.masked_array(yscaler_all.fit_transform(data[:, None])[:,0])
y_transform.mask = mask
pc_all, ica_all = pca_comp(uu, y_transform, 10)
u_all_transform = xscaler.fit_transform(pc_all)

#считаем R на всей выборки
R_score_for_knn(uu, data, yscaler, "все места, fit на реальной выборке", 10, 'knn')

X_train, X_test, Y_train, Y_test = train_test_split(pc_all, y_transform, test_size=0.25)
knn_all = KNeighborsRegressor(n_neighbors=3)
knn_all.fit(X_train,Y_train)
y_pred_test = knn_all.predict(X_test)
x_scaler = yscaler_all.inverse_transform(Y_test[:, None])[:,0]
y_scaler = yscaler_all.inverse_transform(y_pred_test[:, None])

print('все места, fit на реальной выборке', R_squared(x_scaler, y_scaler)),scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')

#ссоздаем новый датасет
X_train, X_test, Y_train, Y_test = train_test_split(y_transform, pc_all, test_size=0.30)
knn_all_create = KNeighborsRegressor(n_neighbors=3)
knn_all_create.fit(X_train[:, np.newaxis], Y_train)
NEW_Y_transform = y_transform
NEW_X = knn_all_create.predict(y_transform[:, np.newaxis])

with open('{}/knn_model_all.pkl'.format(path_save_data[:4]), 'wb') as file:
    pickle.dump(knn_all, file)
with open('{}/ica_all.pkl'.format(path_save_data[:4]), 'wb') as file:
    pickle.dump(ica_all, file)
with open('{}/yscaler_all.pkl'.format(path_save_data[:4]), 'wb') as file:
    pickle.dump(yscaler_all, file)

############################################################################################################################################
############################################################################################################################################
############################################################################################################################################

y_transform_trsk = np.ma.masked_array(yscaler_all.transform(water_merra_trsk[:, None])[:,0])
y_transform_trsk.mask = mask_trsk
pc_trsk =  ica_all.transform(u_trsk)

X_train, X_test, Y_train, Y_test = train_test_split(y_transform_trsk, pc_trsk, test_size=0.30)
knn = KNeighborsRegressor(n_neighbors=3)
knn.fit(X_train[:, np.newaxis], Y_train)
NEW_Y_transform_trsk = y_transform_trsk
NEW_X_trsk = knn.predict(y_transform_trsk[:, np.newaxis])

R_score_knn_fit(NEW_X_trsk, NEW_Y_transform_trsk, yscaler_all, knn_all, "значение R^2 для новых данных Терскола с fit на общей выборкe", 10, path_trsk_new)
np.save('{}/NEW_trsk_miap.npy'.format(path_save_data), NEW_X_trsk)
np.save('{}/NEW_trsk_merra_transform.npy'.format(path_save_data), NEW_Y_transform_trsk.data)
#################################################################################################
y_transform_bdr = np.ma.masked_array(yscaler_all.transform(water_merra_bdr[:, None])[:,0])
y_transform_bdr.mask = mask_bdr
pc_bdr =  ica_all.transform(u_bdr)

X_train, X_test, Y_train, Y_test = train_test_split(y_transform_bdr, pc_bdr, test_size=0.30)
knn = KNeighborsRegressor(n_neighbors=3)
knn.fit(X_train[:, np.newaxis], Y_train)
NEW_Y_transform_bdr = y_transform_bdr
NEW_X_bdr = knn.predict(y_transform_bdr[:, np.newaxis])


R_score_knn_fit(NEW_X_bdr, NEW_Y_transform_bdr, yscaler_all, knn_all, "значение R^2 для новых данных Бадар с fit на общей выборкe", 10, path_bdr_new)
np.save('{}/NEW_bdr_miap.npy'.format(path_save_data), NEW_X_bdr)
np.save('{}/NEW_bdr_merra_transform.npy'.format(path_save_data), NEW_Y_transform_bdr.data)
#################################################################################################
y_transform_sv = np.ma.masked_array(yscaler_all.transform(water_merra_sv[:, None])[:,0])
y_transform_sv.mask = mask_sv
pc_sv = ica_all.transform(u_sv)

X_train, X_test, Y_train, Y_test = train_test_split(y_transform_sv, pc_sv, test_size=0.30)
knn = KNeighborsRegressor(n_neighbors=3)
knn.fit(X_train[:, np.newaxis], Y_train)
NEW_Y_transform_sv = y_transform_sv
NEW_X_sv = knn.predict(y_transform_sv[:, np.newaxis])

R_score_knn_fit(NEW_X_sv,  NEW_Y_transform_sv, yscaler_all, knn_all, "значение R^2 для новых данных Шпицбергена с fit на общей выборкe", 10, path_sv_new)
np.save('{}/NEW_sv_miap.npy'.format(path_save_data), NEW_X_sv)
np.save('{}/NEW_sv_merra_transform.npy'.format(path_save_data), NEW_Y_transform_sv.data)