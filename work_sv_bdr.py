import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 
import netCDF4 as nc
from datetime import timedelta

from scipy import interpolate 
from sklearn.linear_model import LinearRegression, HuberRegressor 


from keras.layers import LSTM, Dense, Conv1D, MaxPooling1D, TimeDistributed
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing

###########################################
#загрузка данных
###########################################
if 'tdats' not in dir(): 
    tdats = [] 
    data = np.zeros((0,12)) 
 
def load(in_name = 'MIAP-2 data/01_Badary',i=0): 
    import datetime 
     
    uc = np.array([10,11,12,13,14,15,16,17,18,19,20,21]) 
    uc2 = uc - 2 
         
    year = 2016 + i%12 
    tdat = [] 
    i = 0 
    for l in open(in_name+'.dat',encoding='CP1251').readlines():
        i+=1 
        tt = l.split('\t')[6] 
        td = tt.split(' ')         
        while '' in td:  
            td.remove('') 
        try: 
            tdat.append(datetime.datetime.strptime(tt,'%d.%m.%Y %H:%M:%S')) 
        except: 
            try: 
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[2]),int(td[3]),int(td[4]))) 
            except: 
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[1]),int(td[2]),int(td[3]))) 
     
            uc2=uc        
     
    data2 = np.loadtxt(in_name+'.dat',usecols=uc2) 
    return(tdat,data2)  

td,data = load('bdr')
tdats = np.array(td,dtype=np.datetime64) 

data[:6] -= data[:6].mean(axis=0) 
data[6:] -= data[6:].mean(axis=0)

data_2016 = pd.read_csv('WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
data_2017 = pd.read_csv('WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)
data_2016[data_2016 == data_2016['wt2'][2697]] = np.nan
data_2016[data_2016 == data_2016['wat1'][3630]] = np.nan
data_2017[data_2017 == data_2017['wt2'][3453]]=np.nan
data_2017[data_2017 == data_2017['wat1'][1460]]=np.nan

timed1 = pd.to_datetime(data_2016['date'] + ' ' + data_2016['time']).to_numpy(dtype=np.datetime64)
timed2 = pd.to_datetime(data_2017['date'] + ' ' + data_2017['time']).to_numpy(dtype=np.datetime64)

water_rad1 = data_2016['wt2'].to_numpy(dtype=float)
water_gnss1 = data_2016['wat1'].to_numpy(dtype=float)
water_rad2 = data_2017['wt2'].to_numpy(dtype=float)
water_gnss2 = data_2017['wat1'].to_numpy(dtype=float)

timed_miap = timed1[:13248]
timed_miap = np.append(timed1[:13248], timed2)
water_rad = water_rad1[:13248]
water_rad = np.append(water_rad1[:13248], water_rad2)
water_gnss = water_gnss1[:13248]
water_gnss = np.append(water_gnss1[:13248], water_gnss2)

water_rad[water_rad < 0] = 0
water_gnss[water_gnss < 0] = 0

df_bdr_merra = 'g4_areaAvgTimeSeries_M2I1NXINT_5_12_4_TQV_20160101_20171231_102E.nc'
ds = nc.Dataset(df_bdr_merra)
water_merra_bdr = ds['M2I1NXINT_5_12_4_TQV'][:]/10
timed_merra_bdr = ds['time'][:].data
timed_merra_bdr = np.array(timed_merra_bdr, dtype='datetime64[s]')

df_bdr_aqua = 'g4_areaAvgTimeSeries_MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean.nc'
ds = nc.Dataset(df_bdr_aqua)
water_bdr_aqua = ds['MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean'][:]
timed_bdr_aqua = ds['time'][:].data
timed_bdr_aqua = np.array(timed_bdr_aqua, dtype='datetime64[s]')

###########################################
#фильтрация
###########################################
def hampel(data, window_size, simg=3):    
    n = len(data)
    new_data = data.copy()
    k = 1.4826
    idx = []

    for i in range((window_size),(n - window_size)):
        r_median = np.median(data[(i - window_size):(i + window_size)]) 
        r_mad  = k * np.median(np.abs(data[(i - window_size):(i + window_size)] - r_median)) 
        
        if (np.abs(data[i] - r_median) > simg * r_mad):
            new_data[i] = r_median #замена выброса
            idx.append(i)

    return new_data, idx

water_gnss, outliers = hampel(water_gnss, 15)

def interp(data, t_old, t_new, t0):
    try:
        f = interpolate.interp1d((t_old-t0)/np.timedelta64(1,'s'), data,kind='linear',bounds_error=False, fill_value='extrapolate')
    except:
        f = interpolate.interp1d((t_old-t0)/np.timedelta64(1,'s'), data.swapaxes(0,1), kind='linear',bounds_error=False, fill_value='extrapolate')
    data_new = f((t_new-t0)/np.timedelta64(1,'s')) 
    data_new = np.ma.array(data_new,mask=np.isnan(data_new))
    data_new[np.isnan(data_new)] = 0
    data_new[data_new==0]=np.ma.masked
    return data_new

merra = interp(water_merra_bdr, timed_merra_bdr, tdats, tdats[0])
gns = interp(water_gnss, timed_miap, tdats, tdats[0])
rrp = interp(water_rad, timed_miap, tdats, tdats[0] )
modis = interp(water_bdr_aqua, timed_bdr_aqua, tdats, tdats[0])

X = data
###########################################
#анализ данных
###########################################
#рисунок всех данных
def plot_time_series(time_data):
    plt.figure(figsize=(13,8))
    for i, values in enumerate(time_data):
        begin_data = []
        begin_data.append(time_data[i][0])
        if len(time_data[i]) == 4:
            plt.plot(time_data[i][0], time_data[i][1], label=time_data[i][2], linewidth=time_data[i][3])
        else:
            plt.plot(time_data[i][0], time_data[i][1], label=time_data[i][2], linewidth=2)
    plt.xlabel('Time', size = 14)
    plt.ylabel('PWV, cm', size = 14)
    plt.ylim(-1,5)
    plt.legend()
    plt.show()

data_plot = [(timed_miap,water_rad, 'radiometr'),
             (timed_miap,water_gnss,'GNS'),
             (timed_merra_bdr, water_merra_bdr, 'MERRA-2 Model'),
      (timed_bdr_aqua,water_bdr_aqua,'MODIS-Aqua')]
plot_time_series(data_plot)

#R^2 оценка
def R_squared(X1,X2):
    lr = LinearRegression()
    try: 
        lr.fit(X1, X2)
        R_2 = lr.score(X1, X2)
    except:
        lr.fit(X1.reshape(-1, 1), X2)
        R_2 = lr.score(X1.reshape(-1, 1), X2)
    return R_2
'''
gns_mask = gns[np.logical_not(gns.mask)]
merra_mask = merra[np.logical_not(gns.mask)]
#R^2 оценка для GNS и MERRA без и с интерполяцией
r_squared = R_squared(gns_mask, merra_mask)
print('Корреляция между GNS и MERRA без интерполяции = ', r_squared)
r_squared = R_squared(gns, merra)
print('Корреляция между GNS и MERRA с интерполяцией = ', r_squared)
'''
  
def scatt_R(x, y, labelx, labely):
    plt.figure(figsize=(13,8))
    plt.xlabel('{}, g/cm$^2$'.format(labelx), size = 14)
    plt.ylabel('{}, g/cm$^2$'.format(labely), size = 14)
    plt.scatter(x, y, s=3, marker='x',alpha=0.25, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  R_squared(x, y))))
    plt.legend(fontsize=15)

###########################################
#уменьшение размерности
###########################################
from sklearn.decomposition import PCA, FastICA, FactorAnalysis

def pca_analize(data):
    plt.figure(figsize=(8,5))
    pca = PCA()
    pca.fit_transform(data)
    pca_variance = pca.explained_variance_
    plt.plot(pca_variance)
    plt.ylabel('Variance ratio')
    plt.xlabel('Principal components')
pca_analize(X)

estimator = HuberRegressor(max_iter=10000) 
def pca_def (data_miap, data_nasa):
    pca = PCA(n_components=8)
    ica = FastICA(n_components=8)
    fa = FactorAnalysis()
    scores_regr = {}
    dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
    for esn in dimr:
        es = dimr[esn]
        es.fit(data_miap)
        pc = es.transform(data_miap)
        for iPC in range(pc.shape[1]):
            estimator = HuberRegressor(max_iter=10000)
            Xr = pc[:,iPC][np.logical_not(data_nasa.mask),None]
            yr = data_nasa[np.logical_not(data_nasa.mask)]
            estimator.fit(Xr, yr)
            scor = estimator.score(Xr, yr)
            scores_regr[esn+"{}".format(iPC)]=scor
            print("Score:",scor," estimator:",esn,' C:',iPC)
    
    pc = ica.transform(data_miap)
    return(pc) 
pc_bdr = pca_def(X, merra)

def pca_comp (data_miap, data_nasa, n_components):
    pca = PCA(n_components=n_components)
    ica = FastICA(n_components=n_components)
    fa = FactorAnalysis()
    scores_regr = {}
    dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
    for esn in dimr:
        es = dimr[esn]
        es.fit(data_miap)
        pc = es.transform(data_miap)
        for iPC in range(pc.shape[1]):
            estimator = HuberRegressor(max_iter=10000)
            Xr = pc[:,iPC][np.logical_not(data_nasa.mask),None]
            yr = data_nasa[np.logical_not(data_nasa.mask)]
            estimator.fit(Xr, yr)
            scor = estimator.score(Xr, yr)
            scores_regr[esn+"{}".format(iPC)]=scor
            print("Score:",scor," estimator:",esn,' C:',iPC)
    
    pc = ica.transform(data_miap)
    return(pc)  
def pca_ (data_miap, data_nasa, n_components):
    pca = PCA(n_components=n_components)
    ica = FastICA(n_components=n_components)
    fa = FactorAnalysis()
    scores_regr = {}
    dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
    for esn in dimr:
        es = dimr[esn]
        es.fit(data_miap)
        pc = es.transform(data_miap)
        for iPC in range(pc.shape[1]):
            estimator = HuberRegressor(max_iter=10000)
            Xr = pc[:,iPC]
            yr = data_nasa
            estimator.fit(Xr, yr)
            scor = estimator.score(Xr, yr)
            scores_regr[esn+"{}".format(iPC)]=scor
            print("Score:",scor," estimator:",esn,' C:',iPC)
    
    pc = ica.transform(data_miap)
    return(pc)  

fig, axs = plt.subplots(ncols=8, nrows=1, figsize=(24, 6))
for i in range(8):
    axs[i].scatter(pc_bdr.swapaxes(0,1)[i], merra, s=3, marker='x', alpha=0.25, 
       label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  
       R_squared(pc_bdr.swapaxes(0,1)[i], merra))))
    axs[i].legend(fontsize=12)
    axs[i].set_xlabel('PC{}'.format(i+1), size = 12)
###########################################
#KNN
###########################################

from sklearn.neighbors import KNeighborsRegressor 
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error ,mean_squared_error, median_absolute_error,confusion_matrix,accuracy_score,r2_score
import sklearn.metrics as metrics
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import TimeSeriesSplit
tscv = TimeSeriesSplit()

knn_regr = KNeighborsRegressor(n_neighbors=3)

X = pc_bdr[np.logical_not(merra.mask)]
y = merra[np.logical_not(merra.mask)]

yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
y_transform = yscaler.fit_transform(y[:, None])[:,0]
X_transform = xscaler.fit_transform(X)

X_train, X_test, Y_train, Y_test = train_test_split(X_transform, y_transform, test_size=0.25)

def knn_pred(x_train, x_test, y_train,y_test):
    knn_regr.fit(x_train,y_train)
    y_pred_test = knn_regr.predict(x_test)
    y_pred_train = knn_regr.predict(x_train)
    return y_pred_test, y_pred_train
    
y_pred_test, y_pred_train = knn_pred(X_train, X_test, Y_train, Y_test)

#R^2 оценка для тестовой выборки и предсказанной
r_squared = R_squared(Y_test, y_pred_test)
print('R^2 оценка для тестовой выборки и предсказанной = ', r_squared)

#рисунок scatter (предсказание тестовой выборки, тестовая выборка)
x_scaler = yscaler.inverse_transform(Y_test.reshape(-1, 1))
y_scaler = yscaler.inverse_transform(y_pred_test.reshape(-1, 1)) 


scatt_R(x_scaler, y_scaler, 'MERRA', 'Предсказание')

print(' test mean squared error knn = ', np.sqrt(metrics.mean_squared_error(Y_test, y_pred_test)), '\n',
      'train mean squared error knn = ', np.sqrt(metrics.mean_squared_error(Y_train, y_pred_train)), '\n',
      'test score = ', knn_regr.score(X_test,Y_test), '\n', 
      'train score = ', knn_regr.score(X_train,Y_train), '\n')

#np.savetxt('water_KNN.dat',water_knn)
#np.savetxt('gns.dat',gns.data)
#np.savetxt('rrp.dat',rrp.data)
#np.savetxt('gns_mask.dat',gns.mask)
#np.savetxt('rrp_mask.dat',rrp.mask)
#np.savetxt('ica.dat', pc_bdr)

#####################################################################################
# reshape данных миап, приведение к одной сетке мерры 
#####################################################################################
def round_time(data, min_round):
    
    t = np.zeros(len(data)).astype(str)
    try: 
        data[0].minute
    except:
        data = pd.to_datetime(data)
    for i in range(len(data)):
        if (data[i].minute * 60 + data[i].second) % (min_round*60) != 0:
            r = round ((data[i].minute * 60 + data[i].second) / (min_round*60))
            s = (r*min_round*60 - (data[i].minute * 60 + data[i].second))
            t[i] = data[i] + timedelta(seconds=s)
        else:
            t[i] = data[i]
    t = np.array(t, dtype='datetime64[s]')
    return t
tt = round_time(tdats, 10)

difdats = np.diff(tt)
count = 0
for i in range(len(difdats)):
    if difdats[i] != 600:
        count +=1
        #print(i, difdats[i])
print(count, np.median(difdats))
#tt_s = round_time(tdats_s, 10)

def new_time(data, minute):
    minute = minute
    delta = np.timedelta64('{}'.format(60*minute), 's')
    new_t = []
    i = 1
    new_t.append(data[0])
    while (new_t [-1] < data[-1]):
        new_t.append(data[0]+i*delta)
        i += 1
    td = pd.to_datetime(new_t)
    td = td.round("min")
    return td

td,data = load('bdr')
tdats = np.array(td,dtype=np.datetime64) 

data[:6] -= data[:6].mean(axis=0) 
data[6:] -= data[6:].mean(axis=0) 

"""
difdats = np.diff(tt_s)
count = 0
for i in range(len(difdats)):
    if difdats[i] != 600:
        count +=1
        print(i, difdats[i])
print(count)
"""      
#Проверка то, что интерполированные данные миап дают корректные результаты
t_round = round_time(new_time(tdats, 10), 10)
X = interp(data, tdats, t_round, t_round[0])
merra = interp(water_merra_bdr, timed_merra_bdr, t_round, t_round[0])

X = X.swapaxes(0,1)
pc_new = pca_def(X, merra)
X = pc_new[np.logical_not(merra.mask)]
y = merra[np.logical_not(merra.mask)] 

yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
y_transform = yscaler.fit_transform(y[:, None])[:,0]
X_transform = xscaler.fit_transform(X)

X_train2, X_test2, Y_train2, Y_test2 = train_test_split(X_transform, y_transform, test_size=0.25)
y_pred_test, y_pred_train = knn_pred(X_train2, X_test2, Y_train2, Y_test2)

r_squared = R_squared(Y_test2, y_pred_test)
print('R^2 для тестовой выборки и предсказанной (миап и мерра с общей временной сеткой) = ', r_squared)

##########################################################################################################################################################################
#Проверка то, что решейпнутые данные миап дают корректные результаты на модели кнн
X = interp(data, tdats, t_round, t_round[0])
merra = interp(water_merra_bdr, timed_merra_bdr, t_round, t_round[0])
t_merra = pd.to_datetime(timed_merra_bdr)
t_miap = pd.to_datetime(t_round)

X = X.swapaxes(0,1)
pc_new = pca_def(X, merra)
X = pc_new[np.logical_not(merra.mask)]

for i in range(len(t_merra)):
    if t_merra[i].year == t_miap[len(t_miap)-1].year and t_merra[i].month == t_miap[len(t_miap)-1].month and t_merra[i].day == t_miap[len(t_miap)-1].day and t_merra[i].hour == t_miap[len(t_miap)-1].hour:
        end = i
for i in range(len(t_merra)):
    if t_merra[i].year == t_miap[0].year and t_merra[i].month == t_miap[0].month and t_merra[i].day == t_miap[0].day and t_merra[i].hour == t_miap[0].hour:
        start = i
t_merra = t_merra[start:end+1]
merra_new = water_merra_bdr[start:end+1]

ind_i = []
ind_j = []
j = 0
k = 0
for i in range(len(t_merra)):
    if len(ind_j) == 0:
        k = 0
    else:
        k = ind_j[-1]
    for j in range(k, len(t_miap)):
        
        if t_merra[i].year == t_miap[j].year and t_merra[i].month == t_miap[j].month  and t_merra[i].day == t_miap[j].day and t_merra[i].hour == t_miap[j].hour and t_merra[i].hour == t_miap[j].hour:
            #print(t_merra[i], t_miap[j])
            ind_i.append(i)
            ind_j.append(j)
            break

merra_new = water_merra_bdr[start:end]
u = np.array(X[ind_j[0]:ind_j[1]])
u = u.reshape(1, (ind_j[1] -ind_j[0]), X.shape[1])
for i in range(1, len(ind_j)-1):
    Y = X[ind_j[i]:ind_j[i+1]].reshape(1, (ind_j[1] -ind_j[0]), X.shape[1])
    u = np.append(u,Y, axis = 0)
    
y = merra_new
yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
y_transform = yscaler.fit_transform(y[:, None])[:,0]
X_transform = xscaler.fit_transform(X)

u_transform = np.array(X_transform[ind_j[0]:ind_j[1]])
u_transform = u_transform.reshape(1, (ind_j[1] -ind_j[0]), X_transform.shape[1])
for i in range(1, len(ind_j)-1):
    Y = X_transform[ind_j[i]:ind_j[i+1]].reshape(1, (ind_j[1] -ind_j[0]), X_transform.shape[1])
    u_transform = np.append(u_transform,Y, axis = 0)

X_train2, X_test2, Y_train2, Y_test2 = train_test_split(u_transform.reshape(u.shape[0], 8*u.shape[1]), y_transform, test_size=0.25)
y_pred_test, y_pred_train = knn_pred(X_train2, X_test2, Y_train2, Y_test2)

r_squared = R_squared(Y_test2, y_pred_test)
print('R^2 оценка для тестовой выборки и предсказанной= ', r_squared)

##########################################################################################################################################################################
#Проверка, что решейпнутые данные миап дают корректные результаты при запуске модели 


############################################################################################################################################################
X = interp(data, tdats, t_round, t_round[0])
X = X.swapaxes(0,1)

t_merra = pd.to_datetime(timed_merra_bdr)
t_merra = t_merra[start:end]
merra_new = water_merra_bdr[start:end]
y = merra_new
yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
y_transform = yscaler.fit_transform(y[:, None])[:,0]

u = np.array(X[ind_j[0]:ind_j[1]])
u = u.reshape(1, (ind_j[1] -ind_j[0]), X.shape[1])
for i in range(1, len(ind_j)-1):
    Y = X[ind_j[i]:ind_j[i+1]].reshape(1, (ind_j[1] -ind_j[0]), X.shape[1])
    u = np.append(u,Y, axis = 0)
    
u_new = u.reshape(u.shape[0], u.shape[1]*u.shape[2])
u_new_bdr = u_new
pc_bdr = pca_comp(u_new, merra_new, 10)
u_new = pc_bdr[np.logical_not(merra_new.mask)]
u_new_transform = xscaler.fit_transform(u_new)
X_train2, X_test2, Y_train2, Y_test2 = train_test_split(u_new_transform, y_transform, test_size=0.25)
y_pred_test, y_pred_train = knn_pred(X_train2, X_test2, Y_train2, Y_test2)
r_squared = R_squared(Y_test2, y_pred_test)
print('R^2 оценка для тестовой выборки и предсказанной= ', r_squared)

'''
pca = PCA()
pca.fit_transform(u_new)
pca_variance = pca.explained_variance_
pca_variance
'''

y = merra_new[2:-2]
y_transform = y_transform[2:-2]

X_ = np.zeros((u_new_transform.shape[0]-4, 5, u_new_transform.shape[1]))
for i in range(5):
    X_[:,i] = u_new_transform[i:u_new_transform.shape[0]-(4)+i]
    
ss = np.array(X_.shape[1:])

X_train, X_test, y_train, y_test = train_test_split(X_, y_transform, test_size=0.25)
ss = np.array(X_.shape[1:])

l = 7
models = np.zeros(l, dtype=object)

for i in range(6,l):
    input_shape = X_.shape
    model = keras.Sequential()
    model.build(input_shape)
    model.add(Dense((i+1)*ss.prod(), activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
    model.add(LSTM(200, activation='relu', return_sequences =True)) # рекурентная нейронная сеть с долгосрочной памятью (обрабатывает данные в виде shape = (xx, xx, xx))
    model.add(layers.Flatten(input_shape=(ss))) # сглаженный слой (объединяет даные в одномерный вектор)
    model.add(Dense((i+1)*ss.prod()**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
    model.add(Dense((i+1)*ss[1]**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
    model.add(Dense(ss[1], activation='softmax'))
    model.add(Dense(1)) # выходной слой
    model.compile(optimizer='adam', loss='mean_absolute_error')
    #model.summary()

    logdir=f"logs/fit/{i}/"
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    model.fit(X_train, y_train, epochs=64, validation_data=(X_test, y_test), callbacks=[tensorboard_callback])

    from keras.utils import plot_model
    plot_model(model, to_file='model.png', show_shapes=True)

    predicted = model.predict(X_test)[:,0]
    y_pred_test = model.predict(X_test)[:,0]
    y_pred_train = model.predict(X_train)[:,0]  


    #смотрю на регрессию между предсказанными данными и данными merra
    x_scaler = yscaler.inverse_transform(y_test[:, None])[:,0]
    y_scaler = yscaler.inverse_transform(predicted[:, None])  
    lr = LinearRegression()
    lr.fit(x_scaler.reshape(-1, 1), y_scaler)
    r_squared = lr.score(x_scaler.reshape(-1, 1), y_scaler)
    print('r^2 = ', r_squared)  

    #рисуноки предсказанного значения и MERRA
    plt.figure(figsize=(13,8))
    plt.scatter(x_scaler, y_scaler[:,0], s=3, marker='x',alpha=0.25)
    plt.xlabel('MERRA, g/cm$^2$', size = 14)
    plt.ylabel('Предсказание, g/cm$^2$', size = 14)
    plt.xlim(0,2)
    plt.ylim(0,2)
    plt.scatter(x_scaler, y_scaler, s=3, marker='x', alpha=0.25, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  r_squared)))
    plt.legend(fontsize=15)
    plt.show() 

    #log-шкала
    plt.figure(figsize=(10,8))
    plt.xlabel('MERRA, g/cm$^2$', size = 14)
    plt.ylabel('Предсказание, g/cm$^2$', size = 14)    
    plt.scatter(x_scaler, y_scaler, s=3, marker='x', alpha=0.25, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  r_squared)))
    plt.legend(fontsize=15)
    plt.semilogx ()
    plt.semilogy ()
    plt.xlim(10**(-1.3))
    plt.show()

plt.figure(figsize=(10,8))
water_ann = model.predict(X_)    
plt.plot(yscaler.inverse_transform(water_ann))
plt.plot(merra_new[2:-2])

for i in range(len(t_merra)):
    if t_merra[i].year == t_gns[len(t_gns)-1].year and t_merra[i].month == t_gns[len(t_gns)-1].month and t_merra[i].day == t_gns[len(t_gns)-1].day and t_merra[i].hour == t_gns[len(t_gns)-1].hour:
        end_gns = i
for i in range(len(t_merra)):
    if t_merra[i].year == t_gns[0].year and t_merra[i].month == t_gns[0].month and t_merra[i].day == t_gns[0].day and t_merra[i].hour == t_gns[0].hour:
        start_gns = i
t_merra_gns = t_merra[start_gns:end_gns]
merra_new_gns = merra_new[start_gns:end_gns]
gns = interp(water_gnss, timed_miap, t_merra_gns, t_round[0])
merra_gns = interp(merra, t_round, t_merra_gns, t_round[0])
gns_mask = gns[np.logical_not(gns.mask)]
merra_mask = merra_gns[np.logical_not(gns.mask)]
scatt_R(gns_mask, merra_mask, 'gns', 'merra')

WaterAnn = yscaler.inverse_transform(water_ann)[start_gns:end_gns]
water_ann_mask = WaterAnn[np.logical_not(gns.mask)]
scatt_R(gns_mask, water_ann_mask, 'gns', 'ANN')

###################################################################################
##########################################################################################
# Данные Шпицбергена вместе с бадарами
#############################################################################################
td,data = load('svalbard')
tdats_s = np.array(td,dtype=np.datetime64)

data[:6] -= data[:6].mean(axis=0)
data[6:] -= data[6:].mean(axis=0)
#data=data.swapaxes(0,1)

df_svalb = 'g4.areaAvgTimeSeries.MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean.20180101-20191231.14E_78N_14E_78N.nc'
ds = nc.Dataset(df_svalb)
water4_svalb = ds['MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean'][:].data
timed4_svalb = ds['time'][:].data
timed4_svalb = np.array(timed4_svalb, dtype='datetime64[s]')

df_svalb = 'g4.areaAvgTimeSeries.M2T1NXSLV_5_12_4_TQV.20180101-20191231.14E_78N_14E_78N.nc'
ds = nc.Dataset(df_svalb)
water3_svalb = ds['M2T1NXSLV_5_12_4_TQV'][:]/10
timed3_svalb = ds['time'][:].data
timed3_svalb = np.array(timed3_svalb, dtype='datetime64[s]')

tt_svalb = round_time(tdats_s, 10)
t_round_svalb = round_time(new_time(tdats_s, 10), 10)

X = interp(data, tdats_s, t_round_svalb, t_round_svalb[0])
merra_svalb = interp(water3_svalb, timed3_svalb, t_round_svalb, t_round_svalb[0])
X = X.swapaxes(0,1)

t_merra_svalb = pd.to_datetime(timed3_svalb)
t_miap_svalb = pd.to_datetime(t_round_svalb)
for i in range(len(t_merra)):
    if t_merra_svalb[i].year == t_miap_svalb[len(t_miap_svalb)-1].year and t_merra_svalb[i].month == t_miap_svalb[len(t_miap_svalb)-1].month and t_merra_svalb[i].day == t_miap_svalb[len(t_miap_svalb)-1].day and t_merra_svalb[i].hour == t_miap_svalb[len(t_miap_svalb)-1].hour and t_merra_svalb[i].minute == t_miap_svalb[len(t_miap_svalb)-1].minute:
        end = i
for i in range(len(t_merra)):
    if t_merra_svalb[i].year == t_miap_svalb[0].year and t_merra_svalb[i].month == t_miap_svalb[0].month and t_merra_svalb[i].day == t_miap_svalb[0].day and t_merra_svalb[i].hour == t_miap_svalb[0].hour:
        start = i
t_merra_svalb = t_merra_svalb[start:end]
t_miap_svalb = t_miap_svalb[1:]
merra_svalb = water3_svalb[start:end]

ind_i = []
ind_j = []
k = 0
for i in range(len(t_merra_svalb)):
    if len(ind_j) == 0:
        k = 0
    else:
        k = ind_j[-1]
    for j in range(k, len(t_miap_svalb)):

        if t_merra_svalb[i].year == t_miap_svalb[j].year and t_merra_svalb[i].month == t_miap_svalb[j].month and t_merra_svalb[i].day == t_miap_svalb[j].day and t_merra_svalb[i].hour == t_miap_svalb[j].hour and t_merra_svalb[i].minute == t_miap_svalb[j].minute:
            #print(t_merra_svalb[i], t_miap_svalb[j])
            ind_i.append(i)
            ind_j.append(j)
            break

u = np.array(X[ind_j[0]:ind_j[1]])
u = u.reshape(1, (ind_j[1] -ind_j[0]), X.shape[1])
for i in range(1, len(ind_j)-1):
    Y = X[ind_j[i]:ind_j[i+1]].reshape(1, (ind_j[1] -ind_j[0]), X.shape[1])
    u = np.append(u,Y, axis = 0)
    



u_new_svalb = u.reshape(u.shape[0], u.shape[1]*u.shape[2])
data = np.append(merra_new.data, merra_svalb.data[:4000])
mask = np.append(merra_new.mask, merra_svalb.mask[:4000])
uu = np.append(u_new_bdr, u_new_svalb[:4000], axis = 0)

pca = PCA(n_components=8)
ica = FastICA(n_components=8)
fa = FactorAnalysis()
scores_regr = {}
dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
for esn in dimr:
    es = dimr[esn]
    es.fit(uu)
    pc = es.transform(uu)
    for iPC in range(pc.shape[1]):
        estimator = HuberRegressor(max_iter=10000)
        Xr = pc[:,iPC][np.logical_not(mask),None]
        yr = data[np.logical_not(mask)]
        estimator.fit(Xr, yr)
        scor = estimator.score(Xr, yr)
        scores_regr[esn+"{}".format(iPC)]=scor
        print("Score:",scor," estimator:",esn,' C:',iPC)
pc_all = ica.transform(uu)
u_all = pc_all[np.logical_not(mask)]
u_all_transform = xscaler.fit_transform(u_all)
y = data
yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
y_transform = yscaler.fit_transform(data[:, None])[:,0]
X_train2, X_test2, Y_train2, Y_test2 = train_test_split(u_all_transform, y_transform, test_size=0.25)
y_pred_test, y_pred_train = knn_pred(X_train2, X_test2, Y_train2, Y_test2)
r_squared = R_squared(Y_test2, y_pred_test)
print('R^2 оценка для тестовой выборки и предсказанной= ', r_squared)


y = data[2:-2]
y_transform = y_transform[2:-2]

X_ = np.zeros((u_all_transform.shape[0]-4, 5, u_all_transform.shape[1]))
for i in range(5):
    X_[:,i] = u_all_transform[i:u_all_transform.shape[0]-(4)+i]
    
ss = np.array(X_.shape[1:])

X_train, X_test, y_train, y_test = train_test_split(X_, y_transform, test_size=0.25)

l = 7
models = np.zeros(l, dtype=object)

for i in range(6,l):
    input_shape = X_.shape
    model = keras.Sequential()
    model.build(input_shape)
    model.add(Dense((i+1)*ss.prod(), activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
    model.add(LSTM(200, activation='relu', return_sequences =True)) # рекурентная нейронная сеть с долгосрочной памятью (обрабатывает данные в виде shape = (xx, xx, xx))
    model.add(layers.Flatten(input_shape=(ss))) # сглаженный слой (объединяет даные в одномерный вектор)
    model.add(Dense((i+1)*ss.prod()**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
    model.add(Dense((i+1)*ss[1]**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
    model.add(Dense(ss[1], activation='softmax'))
    model.add(Dense(1)) # выходной слой
    model.compile(optimizer='adam', loss='mean_absolute_error')
    #model.summary()

    logdir=f"logs/fit/{i}/"
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    model.fit(X_train, y_train, epochs=64, validation_data=(X_test, y_test), callbacks=[tensorboard_callback])

    from keras.utils import plot_model
    plot_model(model, to_file='model.png', show_shapes=True)

    predicted = model.predict(X_test)[:,0]
    y_pred_test = model.predict(X_test)[:,0]
    y_pred_train = model.predict(X_train)[:,0]  


    #смотрю на регрессию между предсказанными данными и данными merra
    x_scaler = yscaler.inverse_transform(y_test[:, None])[:,0]
    y_scaler = yscaler.inverse_transform(predicted[:, None])  
    lr = LinearRegression()
    lr.fit(x_scaler.reshape(-1, 1), y_scaler)
    r_squared = lr.score(x_scaler.reshape(-1, 1), y_scaler)
    print('r^2 = ', r_squared)  

    #рисуноки предсказанного значения и MERRA
    plt.figure(figsize=(13,8))
    plt.scatter(x_scaler, y_scaler[:,0], s=3, marker='x',alpha=0.25)
    plt.xlabel('MERRA, g/cm$^2$', size = 14)
    plt.ylabel('Предсказание, g/cm$^2$', size = 14)
    plt.xlim(0,2)
    plt.ylim(0,2)
    plt.scatter(x_scaler, y_scaler, s=3, marker='x', alpha=0.25, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  r_squared)))
    plt.legend(fontsize=15)
    plt.show() 

    #log-шкала
    plt.figure(figsize=(10,8))
    plt.xlabel('MERRA, g/cm$^2$', size = 14)
    plt.ylabel('Предсказание, g/cm$^2$', size = 14)    
    plt.scatter(x_scaler, y_scaler, s=3, marker='x', alpha=0.25, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  r_squared)))
    plt.legend(fontsize=15)
    plt.semilogx ()
    plt.semilogy ()
    plt.xlim(10**(-1.3))
    plt.show()

plt.figure(figsize=(10,8))
water_ann = model.predict(X_)    
plt.plot(yscaler.inverse_transform(water_ann))
plt.plot(data[2:-2])
scatt_R(water_ann, data[2:-2], "X_", "data")


#######################################################################
