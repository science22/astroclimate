from functions import*
from knn_create_data import*

###################################################################################################################################################################################
"""data = np.append(water_merra_bdr, water_merra_trsk, axis = 0) 
uu = np.append(u_bdr, u_trsk, axis = 0) 
mask = np.append(mask_bdr, mask_trsk)

data = np.append(data, water_merra_sv, axis = 0)
uu = np.append(uu, u_sv, axis = 0) 
mask = np.append(mask, mask_sv)
y_transform = np.ma.masked_array(yscaler.fit_transform(data[:, None])[:,0])
y_transform.mask = mask
pc_all, ica_all = pca_comp(uu, y_transform, 10)"""
###################################################################################################################################################################################

y_transform = np.ma.masked_array(yscaler.transform(water_merra_bdr[:, None])[:,0])
y_transform.mask = mask_bdr
pc_bdr =  ica_all.transform(u_bdr)
u_new_transform = xscaler.fit_transform(pc_bdr)

y_transform = y_transform[2:-2]
X_ = np.zeros((u_new_transform.shape[0]-4, 5, u_new_transform.shape[1]))
for i in range(5):
    X_[:,i] = u_new_transform[i:u_new_transform.shape[0]-(4)+i]
#X_ = u_new_transform.reshape(u_new_transform.shape[0], 1, u_new_transform.shape[1])
input_shape = X_.shape #u_new_transform.shape

#########################################################
'''indices = np.arange((len(y_transform)/2 - 50), (len(y_transform)/2 + 50))
indices = indices.astype(int)

bdr_valid = y_transform[indices]
X_bdr_valid = X_[indices]
y_transform  = np.delete(y_transform, indices)
X_ = np.delete(X_, indices, axis=0)'''
########################################################

pred_test_bdr, pred_train_bdr, history_bdr, model = ann_fit(X_, y_transform, yscaler, 6, path_bdr_ann, input_shape, np.array([1, 10]))

y_pred = model.predict(X_)[:,0] 
x_scaler = yscaler.inverse_transform(y_transform[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred[:, None])
scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')
###################################################################################################################################################################################
y_transform = np.ma.masked_array(yscaler.transform(water_merra_sv[:, None])[:,0])
y_transform.mask = mask_sv
pc_bdr =  ica_all.transform(u_sv)
u_new_transform = xscaler.fit_transform(pc_sv)

y_transform = y_transform[2:-2]
X_ = np.zeros((u_new_transform.shape[0]-4, 5, u_new_transform.shape[1]))
for i in range(5):
    X_[:,i] = u_new_transform[i:u_new_transform.shape[0]-(4)+i]



X_train, X_test, Y_train, Y_test = train_test_split(X_, y_transform, test_size=0.25)

for layer in model.layers:
    layer.trainable = False

model.fit(X_train, Y_train, epochs=64, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])
y_pred_test = model.predict(X_test)[:,0]
y_pred_train = model.predict(X_train)[:,0]  
x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred_test[:, None])
scatt_R(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm', path_sv_ann)



y_pred = model.predict(X_sv_valid)[:,0] 
x_scaler = yscaler.inverse_transform(sv_valid[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred[:, None])
scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')
###################################################################################################################################################################################
y_transform = np.ma.masked_array(yscaler.transform(water_merra_trsk[:, None])[:,0])
y_transform.mask = mask_trsk
pc_bdr =  ica_all.transform(u_trsk)
u_new_transform = xscaler.fit_transform(pc_trsk)

y_transform = y_transform[2:-2]
X_ = np.zeros((u_new_transform.shape[0]-4, 5, u_new_transform.shape[1]))
for i in range(5):
    X_[:,i] = u_new_transform[i:u_new_transform.shape[0]-(4)+i]
    
    
################################################
indices = np.arange((len(y_transform)/2 - 50), (len(y_transform)/2 + 50))
indices = indices.astype(int)

trsk_valid = y_transform[indices]
X_trsk_valid = X_[indices]
y_transform  = np.delete(y_transform, indices)
X_ = np.delete(X_, indices, axis=0)
############################################################



X_train, X_test, Y_train, Y_test = train_test_split(X_, y_transform, test_size=0.25)

model.fit(X_train, Y_train, epochs=64, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])
y_pred_test = model.predict(X_test)[:,0]
y_pred_train = model.predict(X_train)[:,0]  
x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred_test[:, None])
scatt_R(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm', path_trsk_ann)



y_pred = model.predict(X_trsk_valid)[:,0] 
x_scaler = yscaler.inverse_transform(trsk_valid[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred[:, None])
scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')