from functions import*

import netCDF4 as nc
from sklearn.preprocessing import MinMaxScaler
from keras.layers import LSTM, Dense, Conv1D, MaxPooling1D, TimeDistributed
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers

from sklearn.neighbors import KNeighborsRegressor 
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error ,mean_squared_error, median_absolute_error,confusion_matrix,accuracy_score,r2_score
import sklearn.metrics as metrics
from sklearn.model_selection import TimeSeriesSplit
tscv = TimeSeriesSplit()
###########################################
#загрузка данных
###########################################
if 'tdats' not in dir(): 
    tdats = [] 
    data = np.zeros((0,12))  

td,data = load('bdr')
tdats = np.array(td,dtype=np.datetime64) 

data[:6] -= data[:6].mean(axis=0) 
data[6:] -= data[6:].mean(axis=0)
X = data

data_2016 = pd.read_csv('WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
data_2017 = pd.read_csv('WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)
data_2016[data_2016 == data_2016['wt2'][2697]] = np.nan
data_2016[data_2016 == data_2016['wat1'][3630]] = np.nan
data_2017[data_2017 == data_2017['wt2'][3453]]=np.nan
data_2017[data_2017 == data_2017['wat1'][1460]]=np.nan

timed1 = pd.to_datetime(data_2016['date'] + ' ' + data_2016['time']).to_numpy(dtype=np.datetime64)
timed2 = pd.to_datetime(data_2017['date'] + ' ' + data_2017['time']).to_numpy(dtype=np.datetime64)

water_rad1 = data_2016['wt2'].to_numpy(dtype=float)
water_gnss1 = data_2016['wat1'].to_numpy(dtype=float)
water_rad2 = data_2017['wt2'].to_numpy(dtype=float)
water_gnss2 = data_2017['wat1'].to_numpy(dtype=float)

timed_miap = timed1[:13248]
timed_miap = np.append(timed1[:13248], timed2)
water_rad = water_rad1[:13248]
water_rad = np.append(water_rad1[:13248], water_rad2)
water_gnss = water_gnss1[:13248]
water_gnss = np.append(water_gnss1[:13248], water_gnss2)

water_rad[water_rad < 0] = 0
water_gnss[water_gnss < 0] = 0

df_bdr_merra = 'g4_areaAvgTimeSeries_M2I1NXINT_5_12_4_TQV_20160101_20171231_102E.nc'
ds = nc.Dataset(df_bdr_merra)
water_merra_bdr = ds['M2I1NXINT_5_12_4_TQV'][:]/10
timed_merra_bdr = ds['time'][:].data
timed_merra_bdr = np.array(timed_merra_bdr, dtype='datetime64[s]')

df_bdr_oxygen = 'g4.areaAvgTimeSeries.M2IMNXINT_5_12_4_TOX.20160101-20171231.102E_51N_102E_51N.nc' # 'g4.areaAvgDiffTimeSeries.M2IMNXINT_5_12_4_TOX+M2TMNXSLV_5_12_4_TOX.20160101-20171231.102E_51N_102E_51N.nc' 
ds = nc.Dataset(df_bdr_oxygen)
bdr_oxygen = ds['M2IMNXINT_5_12_4_TOX'][:]
timed_bdr_oxygen = ds['time'][:].data
timed_bdr_oxygen = np.array(timed_bdr_oxygen, dtype='datetime64[s]')

############################################################################################################################################################################
#KNN
############################################################################################################################################################################
def knn_pred(X, y, numb):
    from sklearn.preprocessing import MinMaxScaler
    
    yscaler = MinMaxScaler((0,1)) 
    xscaler = MinMaxScaler((-1,1)) 
    y_transform = yscaler.fit_transform(y[:, None])[:,0] 
    X_transform = xscaler.fit_transform(X) 
    X_train, X_test, Y_train, Y_test = train_test_split(X_transform, y_transform, test_size=0.25) 

    knn_regr = KNeighborsRegressor(n_neighbors=numb)    
    knn_regr.fit(X_train,Y_train)
    y_pred_test = knn_regr.predict(X_test)
    y_pred_train = knn_regr.predict(X_train)
    r_squared = R_squared(Y_test, y_pred_test)
    
    
    x_scaler = yscaler.inverse_transform(Y_test.reshape(-1, 1))
    y_scaler = yscaler.inverse_transform(y_pred_test.reshape(-1, 1)) 
    return y_pred_test, y_pred_train, r_squared, x_scaler, y_scaler
##########################################################################################################################################################################
##########################################################################################################################################################################
estimator = HuberRegressor(max_iter=10000) 
"""
merra_oxygen = interp(bdr_oxygen, timed_bdr_oxygen, tdats, tdats[0])
pc_bdr = pca_def(X, merra_oxygen)
X = pc_bdr
y = merra_oxygen

y_pred_test, y_pred_train, r_squared, x_scaler, y_scaler = knn_pred(X, y, 3)
print('R^2 оценка для тестовой выборки и предсказанной (интерполяция до "обрезки" во времени данных меры)= ', r_squared)
"""
tt = round_time(tdats, 10)
'''
difdats = np.diff(tt)
count = 0
for i in range(len(difdats)):
    if difdats[i] != 600:
        count +=1
print(count, np.median(difdats))
'''
#Проверка то, что интерполированные данные миап дают корректные результаты
t_round = round_time(new_time(tdats, 10), 10)
X = interp(data, tdats, t_round, t_round[0])
merra = interp(bdr_oxygen, timed_bdr_oxygen, t_round, t_round[0])

X = X.swapaxes(0,1)
pc_new = pca_def(X, merra)
X = pc_new[np.logical_not(merra.mask)]
y = merra[np.logical_not(merra.mask)]

y_pred_test, y_pred_train, r_squared, x_scaler, y_scaler = knn_pred(X, y, 3)
print('R^2 оценка для тестовой выборки и предсказанной (интерполяция до "обрезки" во времени данных меры) = ', r_squared)

##########################################################################################################################################################################
X = interp(data, tdats, t_round, t_round[0])
merra = interp(bdr_oxygen, timed_bdr_oxygen, t_round, t_round[0])
t_merra = pd.to_datetime(timed_bdr_oxygen) 
t_miap = pd.to_datetime(t_round)

X = X.swapaxes(0,1)
pc_new = pca_def(X, merra)
X = pc_new[np.logical_not(merra.mask)]

for i in range(len(t_merra)): 
    if t_merra[i] == t_miap[len(t_miap)-1406]: 
        end = i
for i in range(len(t_merra)): 
    if t_merra[i].year == t_miap[0].year and t_merra[i].month == t_miap[0].month: 
        start = i 

merra_oxygen = interp(bdr_oxygen[start:end], timed_bdr_oxygen[start:end], t_round, t_round[0]) 
pc_bdr = pca_def(X, merra_oxygen) 
X = X.swapaxes(0,1) 
X = pc_bdr
y = merra_oxygen

y_pred_test, y_pred_train, r_squared, x_scaler, y_scaler = knn_pred(X, y, 3)
print('R^2 оценка для тестовой выборки и предсказанной (интерполяция после "обрезки" во времени данных меры) = ', r_squared)

##########################################################################################################################################################################
#Нейронка
############################################################################################################################################################
X = interp(data, tdats, t_round, t_round[0])
X = X.swapaxes(0,1)
merra_oxygen = interp(bdr_oxygen[start:end], timed_bdr_oxygen[start:end], t_round, t_round[0]) 
t_merra = pd.to_datetime(timed_bdr_oxygen) 
t_miap = pd.to_datetime(t_round)
X = pc_bdr

y = merra_oxygen
yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
y_transform = yscaler.fit_transform(y[:, None])[:,0]
X_transform = xscaler.fit_transform(X)
y_pred_test, y_pred_train, history_loss = model_shape3(X_transform, y_transform, 6, xscaler, yscaler)

#рисуноки предсказанного значения и MERRA
scatt_R(x_scaler, y_scaler, 'MERRA-2', 'Prediction')
plt.show()
'''
X_transform = X_transform.reshape(X_transform.shape[0],1, X_transform.shape[1])
X_train, X_test, Y_train, Y_test = train_test_split(X_transform, y_transform, test_size=0.25)
ss = np.array(X_transform.shape[1:])
input_shape = X_transform.shape
model = keras.Sequential()
model.build(input_shape)
model.add(Dense((i+1)*ss.prod(), activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
model.add(LSTM(200, activation='relu', return_sequences =True)) # рекурентная нейронная сеть с долгосрочной памятью (обрабатывает данные в виде shape = (xx, xx, xx))
model.add(layers.Flatten(input_shape=(ss))) # сглаженный слой (объединяет даные в одномерный вектор)
model.add(Dense((i+1)*ss.prod()**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой/ можно убрать 
model.add(Dense((i+1)*ss[1]**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
model.add(Dense(ss[1], activation='softmax'))
model.add(Dense(1)) # выходной слой
model.compile(optimizer='adam', loss='mean_absolute_error')
#model.summary()

logdir=f"logs/fit/{i}/"
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
history = model.fit(X_train, Y_train, epochs=300, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])

from keras.utils import plot_model
plot_model(model, to_file='model.png', show_shapes=True)
y_pred_test = model.predict(X_test)[:,0]
y_pred_train = model.predict(X_train)[:,0]  
#смотрю на регрессию между предсказанными данными и данными merra
x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
y_scaler = yscaler.inverse_transform(y_pred_test[:, None])  
lr = LinearRegression()
lr.fit(x_scaler.reshape(-1, 1), y_scaler)
r_squared = lr.score(x_scaler.reshape(-1, 1), y_scaler)
print('r^2 = ', r_squared)  

#рисуноки предсказанного значения и MERRA
scatt_R(x_scaler, y_scaler, 'MERRA-2', 'Prediction')
plt.show()
'''
#log-шкала
#scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')
