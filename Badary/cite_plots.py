#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 26 11:48:37 2023

@author: edombek
"""

import matplotlib.pyplot as plt
from  matplotlib import ticker
import numpy as np
import netCDF4 as nc
import pandas as pd
from utils import sliding_window

df = 'g4_areaAvgTimeSeries_M2I1NXINT_5_12_4_TQV_20160101_20171231_102E.nc'
ds = nc.Dataset(df)
water_MERA2 = ds['M2I1NXINT_5_12_4_TQV'][:]/10
timed_MERA2 = ds['time'][:].data
timed_MERA2 = np.array(timed_MERA2, dtype='datetime64[s]')

dataf1 = pd.read_csv('WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
dataf1[dataf1==dataf1['wt2'][2697]]=np.nan
dataf1[dataf1==dataf1['wat1'][3630]]=np.nan
timed1 = pd.to_datetime(dataf1['date']+' '+dataf1['time']).to_numpy(dtype=np.datetime64)
water1 = dataf1['wt2'].to_numpy(dtype=float)
water1[water1<0]=0
dataf = pd.read_csv('WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)
timed = pd.to_datetime(dataf['date']+' '+dataf['time']).to_numpy(dtype=np.datetime64)
dataf[dataf==dataf['wt2'][3453]]=np.nan
water = dataf['wt2'].to_numpy(dtype=float)
water[water<0]=0
timed_wvr = np.append(timed1, timed)
water_wvr = np.append(water1, water)

m=1
if 0:
    timed = timed_wvr
    water_MERA2 = sliding_window(timed_MERA2, water_MERA2, timed)
    mask = np.logical_not(np.isnan(water_MERA2))
    mask = np.logical_and(mask,water_MERA2<m)
    mask = np.logical_and(mask,water_wvr<m)
else:
    timed = timed_MERA2
    water_wvr = sliding_window(timed_wvr, water_wvr, timed)
    mask = np.logical_not(np.isnan(water_wvr))
    mask = np.logical_and(mask,water_MERA2<m)
    mask = np.logical_and(mask,water_wvr<m)
    
corr = np.corrcoef(water_wvr[mask], water_MERA2[mask])

fig, ax = plt.subplots(figsize=(4,4))
p = np.polyfit(water_wvr[mask], water_MERA2[mask], 1)
pf = np.poly1d(p)
ax.scatter(water_wvr[mask], water_MERA2[mask], s=1, color='c', alpha=.4)
ax.set_xlim(0.1,m)
ax.set_ylim(0.1,m)
ax.set_xscale('log')
ax.set_yscale('log')
locmin = ticker.LogLocator(base=10.0,subs=(np.linspace(0.1,1,10)),numticks=2)
ax.xaxis.set_minor_locator(locmin)
ax.xaxis.set_minor_formatter(ticker.NullFormatter())
ax.yaxis.set_minor_locator(locmin)
ax.yaxis.set_minor_formatter(ticker.NullFormatter())
ax.set_xlabel('Water vapor radiometer, cm')
ax.set_ylabel('MERRA-2, cm')
ax.axline([0,pf(0)], [1,pf(1)], color = 'k', linestyle='--', linewidth=1.5, label=f'$R^2={corr[0,1]:.2f}$')
ax.legend()
fig.tight_layout()
fig.savefig('WVR_MERRA2.pdf')

def scatt_trend(x, y, labelx, labely):
    
    try:
        z = np.polyfit (x, y, 1)
        p = np.poly1d (z)
        xx = x[x.argsort()]
    except:
        try:
            z = np.polyfit (x, y.ravel(), 1)
            p = np.poly1d (z)
            xx = x[x.argsort()]
        except:
            z = np.polyfit (x.ravel(), y, 1)
            p = np.poly1d (z)  
            xx = y[y.argsort()]
    
    plt.figure(figsize=(10,10))
    plt.xlabel('{}'.format(labelx), size = 34)
    plt.ylabel('{}'.format(labely), size = 34)
    plt.scatter(x, y, s=4, marker='x',alpha=0.3)
    plt.plot (xx, p(xx), color = 'k', linestyle='-.', linewidth=2, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  R_squared(x, y))))
    plt.semilogx ()
    plt.semilogy ()
    plt.xlim(10**(-1))
    plt.ylim(10**(-1))
    plt.legend(fontsize=28)
    plt.rc('font', size= 30)
    plt.tight_layout(pad=0.1, w_pad=0.5, h_pad=1.0)
    plt.savefig('scatt.pdf')
    plt.show()


scatt_trend(water_wvr[mask], water_MERA2[mask], 'WVR, cm', 'MERA2, cm')
    
    
