#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 18:20:36 2023

@author: tatiana
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import netCDF4 as nc

data_2016 = pd.read_csv('WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
data_2017 = pd.read_csv('WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)

data_2016[data_2016 == data_2016['wt2'][2697]] = np.nan
data_2016[data_2016 == data_2016['wat1'][3630]] = np.nan
data_2017[data_2017 == data_2017['wt2'][3453]]=np.nan
data_2017[data_2017 == data_2017['wat1'][1460]]=np.nan

timed1 = pd.to_datetime(data_2016['date'] + ' ' + data_2016['time']).to_numpy(dtype=np.datetime64)
timed2 = pd.to_datetime(data_2017['date'] + ' ' + data_2017['time']).to_numpy(dtype=np.datetime64)

water_rad1 = data_2016['wt2'].to_numpy(dtype=float)
water_gnss1 = data_2016['wat1'].to_numpy(dtype=float)
water_rad2 = data_2017['wt2'].to_numpy(dtype=float)
water_gnss2 = data_2017['wat1'].to_numpy(dtype=float)

water_rad1[water_rad1 < 0] = 0
water_gnss1[water_gnss1 < 0] = 0
water_rad2[water_rad2 < 0] = 0
water_gnss2[water_gnss2 < 0] = 0

df = 'g4_areaAvgTimeSeries_M2I1NXINT_5_12_4_TQV_20160101_20171231_102E.nc'
ds = nc.Dataset(df)

water3 = ds['M2I1NXINT_5_12_4_TQV'][:]/10
timed3 = ds['time'][:].data
timed3 = np.array(timed3, dtype='datetime64[s]')

df = 'g4_areaAvgTimeSeries_MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean.nc'
ds = nc.Dataset(df)

water4 = ds['MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean'][:]
timed4 = ds['time'][:].data
timed4 = np.array(timed4, dtype='datetime64[s]')

timed_miap = timed1[:13248]
timed_miap = np.append(timed1[:13248], timed2)

water_miap = water_rad1[:13248]
water_miap = np.append(water_rad1[:13248], water_rad2)

water_gnss = water_gnss1[:13248]
water_gnss = np.append(water_gnss1[:13248], water_gnss2)

def hampel(data, window_size, simg=3):    
    n = len(data)
    new_data = data.copy()
    k = 1.4826
    idx = []

    for i in range((window_size),(n - window_size)):
        r_median = np.median(data[(i - window_size):(i + window_size)]) 
        r_mad  = k * np.median(np.abs(data[(i - window_size):(i + window_size)] - r_median)) 
        
        if (np.abs(data[i] - r_median) > simg * r_mad):
            new_data[i] = r_median #замена выброса
            idx.append(i)

    return new_data, idx

water_miap_filter, outliers = hampel(water_miap, 15) # 5 hours
water_gnss_filter, outliers = hampel(water_gnss, 15)

df_gnss = pd.DataFrame(water_gnss_filter, timed_miap)
df_gnss = df_gnss.resample('10min', label='right').asfreq()
df_gnss['linear'] = df_gnss[0].interpolate(method = 'linear')
df_gnss.columns = ['water_gnss', 'linear_gnss']

df_miap = pd.DataFrame(water_miap_filter, timed_miap)
df_miap = df_miap.resample('10min', label='right').asfreq()
df_miap['linear'] = df_miap[0].interpolate(method = 'linear')
df_miap.columns = ['water_miap', 'linear_miap']

df3 = pd.DataFrame(water3, timed3)
df3 = df3.resample('10min', label='right').asfreq()
df3['linear'] = df3[0].interpolate(method = 'linear')
df3.columns = ['water3', 'linear3']

df4 = pd.DataFrame(water4, timed4)
df4 = df4.resample('10min', label='right').asfreq()
df4['linear'] = df4[0].interpolate(method = 'linear')
df4.columns = ['water4', 'linear4']

df_gnss.join(df_miap)
df = df_gnss.join(df_miap)
df = df.join(df3)
df = df.join(df4)

water_miap = df['linear_miap'].to_numpy()
water_gnss = df['linear_gnss'].to_numpy()
water3 = df['linear3'].to_numpy()
water4 = df['linear4'].to_numpy()
timed = df.index.to_numpy()
