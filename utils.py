#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 11:55:28 2023

@author: edombek
"""

import matplotlib.pyplot as plt
import numpy as np

def read_miap_dat(in_name):
    '''
    Функция процессинга одного .dat файла
    '''
    
    from numpy import array, loadtxt
    print("processing "+in_name)

    import datetime
    
    #чтение данных
    
    uc=array((10,11,12,13,14,15,16,17,18,19,20,21)) #выбор колонок для чтения
    uc2 = uc-2 #он менялся
    tdat = []
    
    lines = open(in_name,encoding='CP1251').readlines()
    for l in lines[45:]:
        tt = l.split('\t')[6]
        td = tt.split(' ')
        while '' in td: 
            td.remove('')
        try:
            tdat.append(datetime.datetime.strptime(tt,'%d.%m.%Y %H:%M:%S'))
        except:
            try:
                tdat.append(datetime.datetime(2016,1,int(td[0]),int(td[2]),int(td[3]),int(td[4]))) #формат даты менялся - не можем прочитать один - читаем другой
            except:
                tdat.append(datetime.datetime(2016,1,int(td[0]),int(td[1]),int(td[2]),int(td[3])))
    try:
        return tdat, loadtxt(in_name,usecols=uc2,skiprows=45,encoding='CP1251')
    except:
        return tdat, loadtxt(in_name,usecols=uc2,encoding='CP1251')

#from numba import njit
#@njit(parallel=True)
def sliding_window(x,y,X):
    if np.array(x==X).all(): return y
    Y = np.zeros(X.shape)
    r = np.abs(X[1]-X[0])/2
    d = np.abs(np.diff(X))
    Nd = len(d)
    for i, Xi in enumerate(X):
        mask = np.logical_and(x>Xi-(d[i-1]/2 if i else d[0]/2), x<Xi+(d[-1]/2 if i==Nd else d[i]/2))
        Y[i] = y[mask].mean()
    return Y

def correlations(XY, filename, m=2.5, scatter_alpha=0.25, log = False, line = True):
    N = len(XY)-1
    
    fig, axs = plt.subplots(N,N, figsize=(4*N,4*N), sharex=True, sharey=True)
    for i, xy1 in enumerate(XY):
        for j, xy2 in enumerate(XY):
            if i<=j: continue
            ax = axs[i-1,j]
            x = xy1[0] if np.abs(xy1[0][0]-xy1[0][1]) > np.abs(xy2[0][0]-xy2[0][1]) else xy2[0]
            y1 = sliding_window(*xy1[:2], x)
            y2 = sliding_window(*xy2[:2], x)
            #print(len(y1), len(y2), sum(np.isnan(y1)), sum(np.isnan(y2))) 
            ax.scatter(y1, y2, s=1, color='k', alpha=scatter_alpha)
            if log:
                ax.set_xscale('symlog')
                ax.set_yscale('symlog')
            try:
                ax.set_xlim(right=XY[j][3])
                ax.set_ylim(top=XY[i][3])
            except: pass
            if line:
                ax.axline([0,0], [1,1], color = 'k', linestyle='--', linewidth=1.5)
            if j == 0:ax.set_ylabel(XY[i][2])
            if i == N: ax.set_xlabel(XY[j][2])
            ax.set_xticklabels([])
            ax.set_yticklabels([])
    fig.tight_layout()
    fig.savefig(filename)