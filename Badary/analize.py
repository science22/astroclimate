#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 15 13:52:30 2023

@author: edombek
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import netCDF4 as nc
from sklearn.preprocessing import StandardScaler

if 'tdats' not in dir():
    tdats = []
    data = np.zeros((0,12))


def load(in_name = 'MIAP-2 data/01_Badary',i=0):
    import datetime
    
    uc=np.array((10,11,12,13,14,15,16,17,18,19,20,21))
    uc2 = uc-2
        
    year = 2016+i%12
    tdat = []
    i=0
    for l in open(in_name+'.dat',encoding='CP1251').readlines()[45:]:
        i+=1
        tt = l.split('\t')[6]
        td = tt.split(' ')        
        while '' in td: 
            td.remove('')
        try:
            tdat.append(datetime.datetime.strptime(tt,'%d.%m.%Y %H:%M:%S'))
        except:
            try:
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[2]),int(td[3]),int(td[4])))
            except:
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[1]),int(td[2]),int(td[3])))
    
            uc2=uc
    
            
    
    data2 = np.loadtxt(in_name+'.dat',usecols=uc2, skiprows=45, encoding='CP1251')
    return(tdat,data2)




#чтение миап, бадары
td,dd = load('bdr')
data = dd
td = np.array(td, dtype='datetime64')

scaller = StandardScaler()
data_norm = scaller.fit_transform(data)
#вычитание постоянных составляющих
data=data.swapaxes(0,1)
data[:6] -= data[:6].mean(axis=0)
data[6:] -= data[6:].mean(axis=0)
data=data.swapaxes(0,1)

#чтение и фильтрация независимых источников
dataf1 = pd.read_csv('WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
dataf1[dataf1==dataf1['wt2'][2697]]=np.nan
dataf1[dataf1==dataf1['wat1'][3630]]=np.nan
timed1 = pd.to_datetime(dataf1['date']+' '+dataf1['time']).to_numpy(dtype=np.datetime64)
water1 = dataf1['wt2'].to_numpy(dtype=float)
water21 = dataf1['wat1'].to_numpy(dtype=float)
water1[water1<0]=0
water21[water21<0]=0
dataf = pd.read_csv('WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)
timed = pd.to_datetime(dataf['date']+' '+dataf['time']).to_numpy(dtype=np.datetime64)
dataf[dataf==dataf['wt2'][3453]]=np.nan
dataf[dataf==dataf['wat1'][1460]]=np.nan
water = dataf['wt2'].to_numpy(dtype=float)
water2 = dataf['wat1'].to_numpy(dtype=float)
water[water<0]=0
water2[water2<0]=0

df = 'g4_areaAvgTimeSeries_M2I1NXINT_5_12_4_TQV_20160101_20171231_102E.nc'
ds = nc.Dataset(df)

water3 = ds['M2I1NXINT_5_12_4_TQV'][:]/10
timed3 = ds['time'][:].data
timed3 = np.array(timed3, dtype='datetime64[s]')

df = 'g4_areaAvgTimeSeries_MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean.nc'
ds = nc.Dataset(df)

water4 = ds['MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean'][:]
timed4 = ds['time'][:].data
timed4 = np.array(timed4, dtype='datetime64[s]')

timed = np.append(timed1, timed)
water1 = np.append(water1, water)
water2 = np.append(water21, water2)

dataf = pd.read_csv('../processed/bdr_2016_2017/bdr_tau_filtered_MIAP:IPF.dat',comment='#',keep_default_na=False, delimiter=' ', header=None)
time_miap = pd.to_datetime(dataf[0]+' '+dataf[1]).to_numpy(dtype=np.datetime64)
water_miap2 = dataf[2].to_numpy(dtype=float)
water_miap3 = dataf[3].to_numpy(dtype=float)

c2 = water2[np.isnan(water2)==False].mean()/water_miap2[np.isnan(water_miap2)==False].mean()
c3 = water2[np.isnan(water2)==False].mean()/water_miap3[np.isnan(water_miap3)==False].mean()

#рисовка данных
plt.figure(figsize=(10,6))
plt.plot(timed,water1,label='Water radiometr',linewidth=5)
plt.plot(timed,water2,label='GNSS',linewidth=5)
xlim = plt.xlim()
plt.plot(timed3,water3,label='MMERA-2 Model',linewidth=5)
plt.plot(timed4,water4,label='MODIS-Aqua',linewidth=5)
plt.xlim(xlim)
plt.ylim(-1,5)
plt.xlabel('Time')
plt.ylabel('PWV, cm')
plt.legend()
plt.tight_layout()
plt.savefig('data.png')

from utils import correlations

m = 10
XY = [(timed, water1, 'Water radiometr', m),
      (timed, water2, 'GNSS', m),
      (timed3,water3, 'MMERA-2 Model', m),
      (timed4,water4, 'MODIS-Aqua', m),
      (time_miap,water_miap2, r'MIAP 2mm ($\tau$)', m/c2),
      (time_miap,water_miap3, r'MIAP 3mm ($\tau$)', m/c3)]

correlations(XY, 'correlations.png', log = True)


XY_ = [(td, data_norm[:,0], '3mm 0'),
       (td, data_norm[:,1], '3mm 60.48'),
       (td, data_norm[:,2], '3mm 76.32'),
       (td, data_norm[:,3], '3mm 81.36'),
       (td, data_norm[:,4], '3mm 84.24'),
       (td, data_norm[:,5], '3mm 88.56'),
       (td, data_norm[:,6], '2mm 0'),
       (td, data_norm[:,6], '2mm 60.48'),
       (td, data_norm[:,7], '2mm 76.32'),
       (td, data_norm[:,8], '2mm 81.36'),
       (td, data_norm[:,9], '2mm 84.24'),
       (td, data_norm[:,10], '2mm 88.56')]

correlations(XY_, 'miap_channels_corr.png', m=None, scatter_alpha=0.1, log = True)
