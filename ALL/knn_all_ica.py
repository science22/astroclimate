from functions import*
from knn_all import*


data = np.append(water_merra_bdr, water_merra_trsk, axis = 0) 
data = np.append(data, water_merra_sv, axis = 0)
uu = np.concatenate([u_bdr, u_trsk, u_sv])
mask = np.concatenate([mask_bdr, mask_trsk, mask_sv])

y_transform_all = np.ma.masked_array(yscaler_all.fit_transform(data[:, None])[:,0], mask = mask)
pc_all, ica_all = pca_comp(uu, y_transform_all, 10)
u_all_transform = xscaler.fit_transform(pc_all)
############################################################################################################
y_transform = np.ma.masked_array(yscaler_all.transform(water_merra_trsk[:, None])[:,0], mask = mask_trsk)
pc_trsk =  ica_all.transform(u_trsk)
u_new_transform = xscaler.fit_transform(pc_trsk)
#R_score_for_knn(pc_trsk, y_transform, yscaler_all, 'Терскол', 10, path_trsk)
R_score_for_knn(u_new_transform, y_transform, yscaler_all, 'Терскол', 10, path_trsk)
############################################################################################################
y_transform = np.ma.masked_array(yscaler_all.transform(water_merra_bdr[:, None])[:,0], mask = mask_bdr)
pc_bdr =  ica_all.transform(u_bdr)
u_new_transform = xscaler.fit_transform(pc_bdr)
#R_score_for_knn(pc_bdr, y_transform, yscaler_all, 'Бадары', 10, path_bdr)
R_score_for_knn(u_new_transform, y_transform, yscaler_all, 'Бадары', 10, path_bdr)
############################################################################################################
y_transform = np.ma.masked_array(yscaler_all.transform(water_merra_sv[:, None])[:,0], mask = mask_sv)
pc_sv = ica_all.transform(u_sv)
u_new_transform = xscaler.fit_transform(pc_sv)
#R_score_for_knn(pc_sv, y_transform, yscaler_all, 'Шпицберген', 10, path_sv)
R_score_for_knn(u_new_transform, y_transform, yscaler_all, 'Шпицберген', 10, path_sv)
############################################################################################################
y_transform = np.ma.masked_array(yscaler_all.transform(water_merra_ch[:, None])[:,0], mask = mask_ch)
pc_ch = ica_all.transform(u_ch)
u_new_transform = xscaler.fit_transform(pc_ch)
#R_score_for_knn(pc_sv, y_transform, yscaler_all, 'Шпицберген', 10, path_sv)
R_score_for_knn(u_new_transform, y_transform, yscaler_all, 'Шпицберген', 10, path_ch)
####################################################################################################################################################






