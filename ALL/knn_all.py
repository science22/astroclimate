from functions import*
from load_all_data import*


yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
yscaler_all = MinMaxScaler((0,1))


############################################################################################################
u_trsk, water_merra_trsk, mask_trsk,t_merra_trsk = data_preparation(t_merra_trsk, t_miap_trsk, X_trsk, water_merra_trsk, mask_trsk)

y_transform = np.ma.masked_array(yscaler.fit_transform(water_merra_trsk[:, None])[:,0], mask=mask_trsk)
pc_trsk, ica_trsk = pca_comp(u_trsk, y_transform, 10)
u_new_transform = xscaler.fit_transform(pc_trsk)

R_score_for_knn(u_new_transform, y_transform, yscaler, 'Терскол', 10, path_trsk)
############################################################################################################
u_bdr, water_merra_bdr, mask_bdr, t_merra_bdr = data_preparation(t_merra_bdr, t_miap_bdr, X_bdr, water_merra_bdr, mask_bdr)

y_transform = np.ma.masked_array(yscaler.fit_transform(water_merra_bdr[:, None])[:,0], mask=mask_bdr)
y_transform.mask = mask_bdr
pc_bdr, ica_bdr = pca_comp(u_bdr, y_transform, 10)
u_new_transform = xscaler.fit_transform(pc_bdr)

R_score_for_knn(u_new_transform, y_transform, yscaler, 'Бадары', 10, path_bdr)
############################################################################################################
u_sv, water_merra_sv, mask_sv, t_merra_sv = data_preparation(t_merra_sv, t_miap_sv, X_sv, water_merra_sv, mask_sv)

y_transform = np.ma.masked_array(yscaler.fit_transform(water_merra_sv[:, None])[:,0], mask=mask_sv)
pc_sv, ica_sv = pca_comp(u_sv, y_transform, 10)
u_new_transform = xscaler.fit_transform(pc_sv)

R_score_for_knn(u_new_transform, y_transform, yscaler, 'Шпицберген', 10, path_sv)
############################################################################################################
u_ch, water_merra_ch, mask_ch, t_merra_ch = data_preparation(t_merra_ch, t_miap_ch, X_ch, water_merra_ch, mask_ch)

y_transform = np.ma.masked_array(yscaler.fit_transform(water_merra_ch[:, None])[:,0], mask=mask_ch)
pc_ch, ica_ch = pca_comp(u_ch, y_transform, 10)
u_new_transform = xscaler.fit_transform(pc_ch)

R_score_for_knn(u_new_transform, y_transform, yscaler, 'Чираг', 10, pc_ch)
############################################################################################################

