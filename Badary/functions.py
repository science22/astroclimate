import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta
from scipy import interpolate 
from sklearn.linear_model import LinearRegression, HuberRegressor
from sklearn.decomposition import PCA, FastICA, FactorAnalysis
#from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split


def load(in_name = 'MIAP-2 data/01_Badary',i=0): 
    import datetime 
     
    uc = np.array([10,11,12,13,14,15,16,17,18,19,20,21]) 
    uc2 = uc - 2 
         
    year = 2016 + i%12 
    tdat = [] 
    i = 0 
    for l in open(in_name+'.dat',encoding='CP1251').readlines():
        i+=1 
        tt = l.split('\t')[6] 
        td = tt.split(' ')         
        while '' in td:  
            td.remove('') 
        try: 
            tdat.append(datetime.datetime.strptime(tt,'%d.%m.%Y %H:%M:%S')) 
        except: 
            try: 
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[2]),int(td[3]),int(td[4]))) 
            except: 
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[1]),int(td[2]),int(td[3]))) 
     
            uc2=uc        
     
    data2 = np.loadtxt(in_name+'.dat',usecols=uc2) 
    return(tdat,data2)
    
def interp(data, t_old, t_new, t0):
    try:
        f = interpolate.interp1d((t_old-t0)/np.timedelta64(1,'s'), data,kind='linear',bounds_error=False, fill_value='extrapolate')
    except:
        f = interpolate.interp1d((t_old-t0)/np.timedelta64(1,'s'), data.swapaxes(0,1), kind='linear',bounds_error=False, fill_value='extrapolate')
    data_new = f((t_new-t0)/np.timedelta64(1,'s')) 
    data_new = np.ma.array(data_new,mask=np.isnan(data_new))
    data_new[np.isnan(data_new)] = 0
    data_new[data_new==0]=np.ma.masked
    return data_new

def plot_time_series(time_data):
    plt.figure(figsize=(13,8))
    for i, values in enumerate(time_data):
        begin_data = []
        begin_data.append(time_data[i][0])
        if len(time_data[i]) == 4:
            plt.plot(time_data[i][0], time_data[i][1], label=time_data[i][2], linewidth=time_data[i][3])
        else:
            plt.plot(time_data[i][0], time_data[i][1], label=time_data[i][2], linewidth=2)
    plt.xlabel('Time', size = 14)
    plt.ylabel('PWV, cm', size = 14)
    plt.ylim(-1,5)
    plt.legend()
    plt.show()
    
def R_squared(X1,X2):
    lr = LinearRegression()
    try: 
        lr.fit(X1, X2)
        R_2 = lr.score(X1, X2)
    except:
        lr.fit(X1.reshape(-1, 1), X2)
        R_2 = lr.score(X1.reshape(-1, 1), X2)
    return R_2

def scatt_R(x, y, labelx, labely):
    plt.figure(figsize=(13,8))
    plt.xlabel('{}, cm'.format(labelx), size = 14)
    plt.ylabel('{}, cm'.format(labely), size = 14)
    plt.scatter(x, y, s=3, marker='x',alpha=0.25, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  R_squared(x, y))))
    plt.legend(fontsize=15)
    
def scatt_trend(x, y, labelx, labely):
    
    try:
        z = np.polyfit (x, y, 1)
        p = np.poly1d (z)
        xx = x[x.argsort()]
    except:
        try:
            z = np.polyfit (x, y.ravel(), 1)
            p = np.poly1d (z)
            xx = x[x.argsort()]
        except:
            z = np.polyfit (x.ravel(), y, 1)
            p = np.poly1d (z)  
            xx = y[y.argsort()]
    
    plt.figure(figsize=(10,10))
    plt.xlabel('{}'.format(labelx), size = 34)
    plt.ylabel('{}'.format(labely), size = 34)
    plt.scatter(x, y, s=4, marker='x',alpha=0.3)
    plt.plot (xx, p(xx), color = 'k', linestyle='-.', linewidth=2, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  R_squared(x, y))))
    plt.semilogx ()
    plt.semilogy ()
    plt.xlim(10**(-1))
    plt.ylim(10**(-1))
    plt.legend(fontsize=28)
    plt.rc('font', size= 30)
    plt.tight_layout(pad=0.1, w_pad=0.5, h_pad=1.0)
    plt.savefig('scatt_pred.pdf')
    #plt.show()
    
from sklearn.decomposition import PCA, FastICA, FactorAnalysis

def pca_analize(data):
    plt.figure(figsize=(8,5))
    pca = PCA()
    pca.fit_transform(data)
    pca_variance = pca.explained_variance_
    plt.plot(pca_variance)
    plt.ylabel('Variance ratio')
    plt.xlabel('Principal components')
    
def pca_def (data_miap, data_nasa):
    pca = PCA(n_components=8)
    ica = FastICA(n_components=8)
    fa = FactorAnalysis()
    scores_regr = {}
    dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
    for esn in dimr:
        es = dimr[esn]
        es.fit(data_miap)
        pc = es.transform(data_miap)
        for iPC in range(pc.shape[1]):
            estimator = HuberRegressor(max_iter=10000)
            Xr = pc[:,iPC][np.logical_not(data_nasa.mask),None]
            yr = data_nasa[np.logical_not(data_nasa.mask)]
            estimator.fit(Xr, yr)
            scor = estimator.score(Xr, yr)
            scores_regr[esn+"{}".format(iPC)]=scor
            print("Score:",scor," estimator:",esn,' C:',iPC)
    
    pc = ica.transform(data_miap)
    return(pc) 


def pca_comp (data_miap, data_nasa, n_components):
    pca = PCA(n_components=n_components)
    ica = FastICA(n_components=n_components)
    fa = FactorAnalysis()
    scores_regr = {}
    dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
    for esn in dimr:
        es = dimr[esn]
        es.fit(data_miap)
        pc = es.transform(data_miap)
        for iPC in range(pc.shape[1]):
            estimator = HuberRegressor(max_iter=10000)
            Xr = pc[:,iPC][np.logical_not(data_nasa.mask),None]
            yr = data_nasa[np.logical_not(data_nasa.mask)]
            estimator.fit(Xr, yr)
            scor = estimator.score(Xr, yr)
            scores_regr[esn+"{}".format(iPC)]=scor
            print("Score:",scor," estimator:",esn,' C:',iPC)
    
    pc = ica.transform(data_miap)
    return(pc) 
    

def round_time(data, min_round):
    
    t = np.zeros(len(data)).astype(str)
    try: 
        data[0].minute
    except:
        data = pd.to_datetime(data)
    for i in range(len(data)):
        if (data[i].minute * 60 + data[i].second) % (min_round*60) != 0:
            r = round ((data[i].minute * 60 + data[i].second) / (min_round*60))
            s = (r*min_round*60 - (data[i].minute * 60 + data[i].second))
            t[i] = data[i] + timedelta(seconds=s)
        else:
            t[i] = data[i]
    t = np.array(t, dtype='datetime64[s]')
    return t

def new_time(data, minute):
    minute = minute
    delta = np.timedelta64('{}'.format(60*minute), 's')
    new_t = []
    i = 1
    new_t.append(data[0])
    while (new_t [-1] < data[-1]):
        new_t.append(data[0]+i*delta)
        i += 1
    td = pd.to_datetime(new_t)
    td = td.round("min")
    return td

def model_shape3(X_transform,y_transform,i, xscaler, yscaler):
    from tensorflow import keras
    from keras.layers import LSTM, Dense, Conv1D, MaxPooling1D, TimeDistributed
    from tensorflow import keras
    from tensorflow.keras import layers
    
    X_transform = X_transform.reshape(X_transform.shape[0],1, X_transform.shape[1])
    X_train, X_test, Y_train, Y_test = train_test_split(X_transform, y_transform, test_size=0.25)
    ss = np.array(X_transform.shape[1:])
    input_shape = X_transform.shape
    model = keras.Sequential()
    model.build(input_shape)
    model.add(Dense((i+1)*ss.prod(), activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
    model.add(LSTM(200, activation='relu', return_sequences =True)) # рекурентная нейронная сеть с долгосрочной памятью (обрабатывает данные в виде shape = (xx, xx, xx))
    model.add(layers.Flatten(input_shape=(ss))) # сглаженный слой (объединяет даные в одномерный вектор)
    model.add(Dense((i+1)*ss.prod()**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой/ можно убрать 
    model.add(Dense((i+1)*ss[1]**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
    model.add(Dense(ss[1], activation='softmax'))
    model.add(Dense(1)) # выходной слой
    model.compile(optimizer='adam', loss='mean_absolute_error')
    #model.summary()
    
    logdir=f"logs/fit/{i}/"
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    history = model.fit(X_train, Y_train, epochs=300, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])
    
    from keras.utils import plot_model
    plot_model(model, to_file='model.png', show_shapes=True)
    y_pred_test = model.predict(X_test)[:,0]
    y_pred_train = model.predict(X_train)[:,0]  
    #смотрю на регрессию между предсказанными данными и данными merra
    x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
    y_scaler = yscaler.inverse_transform(y_pred_test[:, None])  
    lr = LinearRegression()
    lr.fit(x_scaler.reshape(-1, 1), y_scaler)
    r_squared = lr.score(x_scaler.reshape(-1, 1), y_scaler)
    print('r^2 = ', r_squared) 
    return y_pred_test, y_pred_train, history
