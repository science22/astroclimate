#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 15:31:41 2023

@author: edombek
"""

import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import pandas as pd

df_ch = 'data/Chirag/g4.areaAvgTimeSeries.M2T1NXSLV_5_12_4_TQV.20130101-20230101.47E_41N_47E_41N.nc'
ds = nc.Dataset(df_ch)
water_merra_ch = ds['M2T1NXSLV_5_12_4_TQV'][:]/10
timed_merra_ch = ds['time'][:].data
t_merra_ch = pd.to_datetime(timed_merra_ch, unit='s')


fig, ax = plt.subplots(figsize=(10,6))
ax.set_xlabel('Time, H')
ax.set_ylabel('PWV MERA-2, cm')
ax.hist2d(t_merra_ch.hour, water_merra_ch, bins=(24, 32))


fig, axs = plt.subplots(ncols=4,figsize=(8*4,10), sharey=True)
axs[0].set_ylabel('PWV MERA-2, см')
for season, season_name in enumerate(('Зима', 'Весна', 'Лето', 'Осень')):
    ax = axs[season]
    mask = (t_merra_ch.month%12 // 3 + 1) == season + 1
    ax.set_xlabel('Время, час')
    ax.hist2d(t_merra_ch.hour[mask], water_merra_ch[mask], bins=(24, 32), range=((0,24),(0,3.5)))
    ax.set_title(season_name)
fig.tight_layout()
fig.savefig('chirag_mera_seasons.png')


fig, axs = plt.subplots(ncols=12,figsize=(8*12,10), sharey=True)
axs[0].set_ylabel('PWV MERA-2, см')
for mounth in range(12):
    ax = axs[mounth]
    mask = t_merra_ch.month%12 == mounth
    ax.set_xlabel('Время, час')
    ax.hist2d(t_merra_ch.hour[mask], water_merra_ch[mask], bins=(24, 32), range=((0,24),(0,3.5)))
    ax.set_title(str(mounth))
fig.tight_layout()
fig.savefig('chirag_mera_mounth.png')