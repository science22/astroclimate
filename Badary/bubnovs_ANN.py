#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  4 15:57:27 2023

@author: edombek
"""

import numpy as np
from utils import read_miap_dat
import pandas as pd
from sklearn.linear_model import HuberRegressor
from sklearn.decomposition import PCA, KernelPCA, FastICA, FactorAnalysis
from numpy import ma
from sklearn.preprocessing import StandardScaler, MinMaxScaler

td,data = read_miap_dat('Badary/bdr.dat')
tdats = np.array(td,dtype=np.datetime64) 

# =============================================================================
# data[:,:6] -= data[:,:6].mean(axis=1)[:,None]
# data[:,6:] -= data[:,6:].mean(axis=1)[:,None]
# =============================================================================

data=data.swapaxes(0,1)
data[:6] -= data[:6].mean(axis=0)
data[6:] -= data[6:].mean(axis=0)
data=data.swapaxes(0,1)

data_2016 = pd.read_csv('Badary/WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
data_2017 = pd.read_csv('Badary/WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)
data_2016[data_2016 == data_2016['wt2'][2697]] = np.nan
data_2016[data_2016 == data_2016['wat1'][3630]] = np.nan
data_2017[data_2017 == data_2017['wt2'][3453]]=np.nan
data_2017[data_2017 == data_2017['wat1'][1460]]=np.nan

water1 = data_2016['wat1'].to_numpy(dtype=float)
water2 = data_2017['wat1'].to_numpy(dtype=float)

water2[water2<0] = np.nan

timed1 = pd.to_datetime(data_2016['date'] + ' ' + data_2016['time']).to_numpy(dtype=np.datetime64)
timed2 = pd.to_datetime(data_2017['date'] + ' ' + data_2017['time']).to_numpy(dtype=np.datetime64)

timed_miap = np.append(timed1[:13248], timed2)

from scipy import interpolate
f2 = interpolate.interp1d((timed2-tdats[0])/np.timedelta64(1,'s'), water2,kind='linear',bounds_error=False)

gns = f2((tdats-tdats[0])/np.timedelta64(1,'s'))
gns = ma.array(gns,mask=np.isnan(gns))

scaler = StandardScaler()
scaler = MinMaxScaler((-1,1))
gns_scaler = StandardScaler()
if 0:
    X = data
else:
    X = scaler.fit_transform(data)

gns_scaled = gns_scaler.fit_transform(gns[:,None])[:,0]

pca = PCA(n_components=8)
pc = pca.fit_transform(X)

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import matplotlib.pyplot as plt
import matplotlib as mpl
tf.keras.backend.clear_session()
#tf.config.threading.set_inter_op_parallelism_threads(os.cpu_count()-2)

s = 2
X = pc[np.logical_not(gns.mask)]
y = gns[np.logical_not(gns.mask)][s:-s]

yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
y_ = yscaler.fit_transform(y[:, None])[:,0]
X_ = xscaler.fit_transform(X)

X1_ = np.zeros((X.shape[0]-s*2, s*2+1, X.shape[1]))
for i in range(s*2+1):
    X1_[:,i] = X_[i:X.shape[0]-(2*s)+i]


X_train, X_test, y_train, y_test = train_test_split(X1_, y_, test_size=0.25)

ss = np.array(X1_.shape[1:])

l = 3
p = np.zeros(l)
models = np.zeros(l, dtype=object)

for i in range(l):
    model = keras.Sequential([
          layers.Flatten(input_shape=(ss)),
          layers.Dense((i+1)*ss.prod()**2, activation='relu',
                       kernel_initializer='random_uniform',
                       bias_initializer='zeros'),
          layers.Dense((i+1)*ss[1]**2, activation='tanh',
                       kernel_initializer='random_uniform',
                       bias_initializer='zeros'),
          layers.Dense(1)
    ])
    
    model.compile(
        optimizer='adam',
        loss='mean_absolute_error',#mean_squared_error/mean_absolute_error/mean_squared_logarithmic_error
        #metrics=['MeanAbsoluteError']
        )
    
    model.summary()
    logdir=f"logs/fit/{i}/"
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    model.fit(X_train, y_train, epochs=256, callbacks=[tensorboard_callback])
    
    predicted = model.predict(X_test)[:,0]
    plt.figure()
    #plt.hist2d(y_test,predicted,density=True,cmin=0.5,bins=128,zorder=2,
    #           range=((0,1),(0,1)),norm=mpl.colors.LogNorm())
    plt.scatter(yscaler.inverse_transform(y_test[:, None])[:,0], yscaler.inverse_transform(predicted[:, None])[:,0], s = 1, marker='x',zorder=3,alpha=0.25)
    #plt.title(f'{i+1}')
    plt.xlabel('GNSS, g/cm$^2$')
    plt.ylabel('Предсказание, g/cm$^2$')
    plt.xlim(0,2)
    plt.ylim(0,2)
    plt.pause(1)
    plt.savefig('figs/{:02.0f}.png'.format(i), dpi = 100)
    plt.close()
    if np.diff(predicted).sum():
        p[i] = model.evaluate(X_test, y_test)
        models[i] = model
        print(f'{i} accuracy: {p[i]}')