#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 11:53:55 2023

@author: edombek
"""

import os
import matplotlib.pyplot as plt
from utils import read_miap_dat
from utils import correlations
import numpy as np
from sklearn.decomposition import PCA
import sys

if 0:
    folder = sys.argv[1]
else:
    folder = './Chirag/'

data = np.zeros((0,12))
t = np.zeros(0)

if 0:
    for f in os.listdir(folder):
        if f.split('.')[-1] != 'dat': continue
        print(f)
        try:
            t_, data_, = read_miap_dat(folder+f)
        except:
            print(f'не прочитан {f}')
        data = np.append(data, data_, axis=0)
        t = np.append(t, np.array(t_))
else:
    t, data, = read_miap_dat('./Chirag/miap_2022_full_filtered.dat')
    t = np.array(t)
    data = np.array(data)
    
timed = []
for ti in t:
    timed.append(ti.timestamp())
timed = np.array(timed)
ii = timed.argsort()

t = t[ii]
timed = timed[ii]
data = data[ii]

data_norm = data.copy()
data_norm[:,:6] -= data_norm[:,:6].mean(axis=0)
data_norm[:,6:] -= data_norm[:,6:].mean(axis=0)

data3mm = data_norm[:,:6]
data2mm = data_norm[:,6:]

pca1 = PCA(n_components=3)
pca2 = PCA(n_components=8)

pc = pca1.fit_transform(data_norm[:,:6]) # only3mm
#pc = pca1.fit_transform(data_norm[:,6:]) # only2mm
#pc = pca2.fit_transform(data_norm)

from sklearn.cluster import DBSCAN

db = DBSCAN(eps=0.01, min_samples=data.shape[0]//50).fit(pc)
labels = db.labels_

# Number of clusters in labels, ignoring noise if present.
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
n_noise_ = list(labels).count(-1)

mask = labels == 0

print("Estimated number of clusters: %d" % n_clusters_)
print("Estimated number of noise points: %d" % n_noise_)

fig, ax = plt.subplots(figsize=(16,6))
ax.plot(t, data3mm, 'g', label = '3mm')
#ax.plot(t, data2mm, 'g', label = '2mm')
ax.plot(t[mask], data3mm[mask], 'k', label = '3mm masked')
#ax.plot(t[mask], data2mm[mask], 'k', label = '2mm masked')
ax.legend()
fig.tight_layout()
fig.savefig(folder+'.png')

XY = [(timed, pc[:,i], f'pc{i}') for i in range(pc.shape[1])]
correlations(XY, folder+'corr.png', log = False, scatter_alpha=0.75)
try:
    XY = [(timed[mask], pc[mask,i], f'pc{i}') for i in range(pc.shape[1])]
    correlations(XY, folder+'corr_masked.png', log = False, scatter_alpha=0.75)
except: pass
#plt.close('all')

np.savetxt(folder+'.mask', mask)

if 1:
    lines_full = np.array(open('./Chirag/miap_2022_full_filtered.dat', encoding='cp1251').readlines())
    lines_full[45:][np.logical_not(mask)]=''
    string = ''.join(lines_full)
    open('./Chirag/miap_2022_full_filtered.dat', 'w', encoding='cp1251').write(string)
