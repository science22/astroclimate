from functions import*

import numpy as np
import pandas as pd
import netCDF4 as nc
from datetime import timedelta
import os
############################################################################################################
if 'tdats' not in dir(): 
    tdats = [] 
    data = np.zeros((0,12))
    
############################################################################################################    
############################################################################################################
path_bdr = 'knn_results/Badary'
path_trsk = 'knn_results/Terskol'
path_sv = 'knn_results/Svalbard'
path_ch = 'knn_results/Chirag'
path_bdr_ann = 'ann_results/Badary'
path_trsk_ann = 'ann_results/Terskol'
path_sv_ann = 'ann_results/Svalbard'
path_ch_ann = 'ann_results/Chirag'
path_save_data = 'data/new_data'
path = [path_bdr, path_trsk, path_sv,path_ch, path_bdr_ann, path_trsk_ann, path_sv_ann, path_ch_ann, path_save_data]
for i, path_dir in enumerate(path):
    if not os.path.exists(path_dir):
        os.makedirs(path_dir)

############################################################################################################
print('Терскол')
td_trsk, data_trsk = load('data/Terscol/2020_terscol')
tdats_trsk = np.array(td_trsk, dtype=np.datetime64)
data_trsk=data_trsk.swapaxes(0,1)
standardized_2mm, min_2mm_trsk, max_val_2mm_trsk  = standardize_data(data_trsk[:6])
standardized_3mm, min_3mm_trsk, max_3mm_trsk = standardize_data(data_trsk[6:])
data_trsk = np.append(standardized_2mm, standardized_3mm, axis = 0)


df_trsk_merra = 'data/Terscol/g4.areaAvgTimeSeries.M2I1NXINT_5_12_4_TQV.20200420-20200727.43E_42N_43E_42N.nc'
ds = nc.Dataset(df_trsk_merra)
water_merra_trsk = ds['M2I1NXINT_5_12_4_TQV'][:]/10
timed_merra_trsk = ds['time'][:].data
timed_merra_trsk = np.array(timed_merra_trsk, dtype='datetime64[s]')

t_round_trsk = round_time(tdats_trsk, 10)
X_trsk = interp(data_trsk, tdats_trsk, t_round_trsk, t_round_trsk[0])

t_merra_trsk = pd.to_datetime(timed_merra_trsk)
t_miap_trsk = pd.to_datetime(round_time(new_time(tdats_trsk, 10), 10))
t_miap_trsk = t_miap_trsk[:-1]

X_trsk,t_trsk,mask_trsk = new_data(t_round_trsk, X_trsk)

start, end = time_frame(t_merra_trsk, t_miap_trsk)
t_merra_trsk = t_merra_trsk[start+1:end]
water_merra_trsk = water_merra_trsk[start+1:end]
X_trsk = X_trsk.swapaxes(0,1)

##########################################################################################################
print('Бадары')
td_bdr, data_bdr = load('data/Badary/bdr')
tdats_bdr = np.array(td_bdr, dtype=np.datetime64) 
data_bdr=data_bdr.swapaxes(0,1)
standardized_2mm, min_2mm_bdr, max_val_2mm_bdr  = standardize_data(data_bdr[:6])
standardized_3mm, min_3mm_bdr, max_3mm_bdr = standardize_data(data_bdr[6:])
data_bdr = np.append(standardized_2mm, standardized_3mm, axis = 0)

df_bdr_merra = 'data/Badary/g4_areaAvgTimeSeries_M2I1NXINT_5_12_4_TQV_20160101_20171231_102E.nc'
ds = nc.Dataset(df_bdr_merra)
water_merra_bdr = ds['M2I1NXINT_5_12_4_TQV'][:]/10
timed_merra_bdr = ds['time'][:].data
timed_merra_bdr = np.array(timed_merra_bdr, dtype='datetime64[s]')

t_round_bdr = round_time(tdats_bdr, 10)
X_bdr = interp(data_bdr, tdats_bdr, t_round_bdr, t_round_bdr[0])

t_merra_bdr = pd.to_datetime(timed_merra_bdr)
t_miap_bdr = pd.to_datetime(round_time(new_time(tdats_bdr, 10), 10))
t_miap_bdr = t_miap_bdr[:-1]

X_bdr,t_bdr,mask_bdr = new_data(t_round_bdr, X_bdr)
start, end = time_frame(t_merra_bdr, t_miap_bdr)
t_merra_bdr = t_merra_bdr[start+1:end]
water_merra_bdr = water_merra_bdr[start+1:end]
X_bdr = X_bdr.swapaxes(0,1)
############################################################################################################
print('Щпицберген')
td_sv, data_sv = load('data/Svalbard/svalbard')
tdats_sv = np.array(td_sv, dtype=np.datetime64) 
data_sv=data_sv.swapaxes(0,1)
standardized_2mm, min_2mm_sv, max_val_2mm_sv  = standardize_data(data_sv[:6])
standardized_3mm, min_3mm_sv, max_3mm_sv = standardize_data(data_sv[6:])
data_sv = np.append(standardized_2mm, standardized_3mm, axis = 0)

df_sv = 'data/Svalbard/g4.areaAvgTimeSeries.M2T1NXSLV_5_12_4_TQV.20180101-20191231.14E_78N_14E_78N.nc'
ds = nc.Dataset(df_sv)
water_merra_sv = ds['M2T1NXSLV_5_12_4_TQV'][:]/10
timed_merra_sv = ds['time'][:].data
timed_merra_sv = np.array(timed_merra_sv, dtype='datetime64[s]')

t_round_sv = round_time(tdats_sv, 10)
X_sv = interp(data_sv, tdats_sv, t_round_sv, t_round_sv[0])
t_merra_sv = pd.to_datetime(timed_merra_sv)
t_miap_sv = pd.to_datetime(round_time(new_time(tdats_sv, 10), 10))
X_sv,t_sv,mask_sv = new_data(t_round_sv, X_sv)
X_sv = X_sv.swapaxes(0,1)

start, end = time_frame(t_merra_sv, t_miap_sv)     
t_merra_sv = t_merra_sv[start+1:end]
water_merra_sv = water_merra_sv[start+1:end]
############################################################################################################
#Чираг
############################################################################################################
print('Чираг')
td_ch,data_ch = load('data/Chirag/chirag')
tdats_ch = np.array(td_ch,dtype=np.datetime64) 
data_ch = data_ch.swapaxes(0,1)
standardized_2mm, min_2mm_ch, max_val_2mm_ch  = standardize_data(data_ch[:6])
standardized_3mm, min_3mm_ch, max_3mm_ch = standardize_data(data_ch[6:])
data_ch = np.append(standardized_2mm, standardized_3mm, axis = 0)

df_ch = 'data/Chirag/g4.areaAvgTimeSeries.M2T1NXSLV_5_12_4_TQV.20220901-20230430.47E_41N_47E_41N.nc'
ds = nc.Dataset(df_ch)
water_merra_ch = ds['M2T1NXSLV_5_12_4_TQV'][:]/10
timed_merra_ch = ds['time'][:].data
timed_merra_ch = np.array(timed_merra_ch, dtype='datetime64[s]')

t_round_ch = round_time(tdats_ch, 10)
X_ch = interp(data_ch, tdats_ch, t_round_ch, t_round_ch[0])
t_merra_ch = pd.to_datetime(timed_merra_ch)
t_miap_ch = pd.to_datetime(round_time(new_time(tdats_ch, 10), 10))
X_ch,t_ch,mask_ch = new_data(t_round_ch, X_ch)
X_ch = X_ch.swapaxes(0,1)

start, end = time_frame(t_merra_ch, t_miap_ch)     
t_merra_ch = t_merra_ch[start+1:end]
water_merra_ch = water_merra_ch[start+1:end]
############################################################################################################
############################################################################################################
############################################################################################################
"""data_2016 = pd.read_csv('data/Badary/WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
data_2017 = pd.read_csv('data/Badary/WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)
data_2016[data_2016 == data_2016['wt2'][2697]] = np.nan
data_2016[data_2016 == data_2016['wat1'][3630]] = np.nan
data_2017[data_2017 == data_2017['wt2'][3453]]=np.nan
data_2017[data_2017 == data_2017['wat1'][1460]]=np.nan

timed1 = pd.to_datetime(data_2016['date'] + ' ' + data_2016['time']).to_numpy(dtype=np.datetime64)
timed2 = pd.to_datetime(data_2017['date'] + ' ' + data_2017['time']).to_numpy(dtype=np.datetime64)

water_rad1 = data_2016['wt2'].to_numpy(dtype=float)
water_gnss1 = data_2016['wat1'].to_numpy(dtype=float)
water_rad2 = data_2017['wt2'].to_numpy(dtype=float)
water_gnss2 = data_2017['wat1'].to_numpy(dtype=float)

timed_miap = timed1[:13248]
timed_miap = np.append(timed1[:13248], timed2)
water_rad = water_rad1[:13248]
water_rad = np.append(water_rad1[:13248], water_rad2)
water_gnss = water_gnss1[:13248]
water_gnss = np.append(water_gnss1[:13248], water_gnss2)

water_rad[water_rad < 0] = 0
water_gnss[water_gnss < 0] = 0

water_gnss, outliers = hampel(water_gnss, 15)
t_round_bdr_gnss = round_time(timed_miap, 10)
t_bdr_gnss = pd.to_datetime(t_round_bdr_gnss)
gns = interp(water_gnss, timed_miap, t_round_bdr_gnss, t_round_bdr_gnss[0])

#gnss = gns[np.logical_not(gns.mask)]
#t_bdr_gnss= t_bdr_gnss[np.logical_not(gns.mask)]
ind_merra, ind_miap = same_time(t_merra_bdr,t_bdr_gnss)
merra = water_merra_bdr[ind_merra]
gns = gns[ind_miap]

r_squared = R_squared(gns, merra)

print('Корреляция между GNS и MERRA с интерполяцией = ', r_squared)"""