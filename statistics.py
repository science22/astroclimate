#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 13:02:36 2022

@author: pzemlyan, mailto:petez@ipfran.ru

"""

from statsmodels.robust import mad as stand_mad
from scipy import stats
import numpy as np
from numpy.linalg import norm
from numpy import array,loadtxt,zeros,savetxt,cos,linspace,exp

from scipy.constants import c,h,k,pi
import numpy as np
from math import log, atan, sin, sqrt
from enum import Enum
from astropy import units as u;
from lmfit import minimize, Parameters, Parameter, report_fit
import pywt

from matplotlib.pyplot import figure,subplot,legend,plot,savefig,ylabel,xlabel, gca, fill_between, ylim

import pandas as pd
from numpy import ma, isnan, median

import glob

heights = {
    "Гуниб":1750,
    "Чираг":2270,
    "Вихли":2850,
    "БТА":2100,
    "ГБС":1750,
    "Маяк":2420
    }

coord = {
    "Гуниб":(42.40278,46.921515),
    "Чираг":(41.837293,47.433969),
    "Вихли":(42.082876,47.291658),
    "БТА":(43.276100, 42.499423),
    "ГБС":(42.40278,46.921515),
    "Маяк":(42.397046,46.878661)
    }


markers = {
    "Гуниб":'.',
    "Чираг":"v",
    "Вихли":'1',
    "БТА":'s',
    "ГБС":'.',
    "Маяк":'+'
    }

size = {
    "Гуниб":20,
    "Чираг":15,
    "Вихли":40,
    "БТА":15,
    "ГБС":20,
    "Маяк":40
    }


zorder = {
    "Гуниб":1,
    "Чираг":2,
    "Вихли":3,
    "БТА":3,
    "ГБС":2,
    "Маяк":3
    }

colors = {
    "Гуниб":'r',
    "Чираг":"g",
    "Вихли":'b',
    "БТА":'c',
    "ГБС":'r',
    "Маяк":'k'
    }

def main(args):

    files = glob.glob('processed/'+args[0]+'/*tau*.dat')
    
    stat = {} #словарь со статистикой
    taus = {} #словарь имя файла: массив с оценками оптиеской толщины
    time = {} #словарь имя файла: массив с временными отсчетами
    
    meteo = {} #словарь с массивами метеоданных 
    from dateutil.parser import parse 
    for f in files:
        fl = open(f)
        lines = fl.readlines()
        key=lines[6][12:-4]
        if key[:3] == "ЧИР":
            key=key[0].upper()+key[1:].lower()
        times = []
        mkey = key[:3]
        for k in heights:
            if mkey in k:
                mkey = k
                break

        for l in lines[8:]:
            times.append(np.datetime64(parse(l[:20],dayfirst=True))) #если в одном месте несколько раз наблюдали, соединим в один файл
        
        tau = ma.array(loadtxt(f,usecols=[2,3,4,5]))
        tau[tau==0]=ma.masked
        tau[isnan(tau)]=ma.masked
        if key in stat and  mkey == stat[key]['name']:
            tau = np.concatenate((taus[key],tau),axis=0)
            
        qtau3 = (tau[:,2]/tau[:,0])
        qtau2 = (tau[:,3]/tau[:,1])
        stat[mkey]={
            'source':lines[0],'name':mkey,#'file':f,
            '2mm mean':"{0:0.3f}".format(tau[:,1].mean()),'3mm':"{0:0.3f}".format(tau[:,0].mean()),'n':tau.shape[0],
            'q1:3mm':"{0:0.3f}".format(np.percentile(tau[~tau.mask[:,1],1],25)),'q1:2mm':"{0:0.3f}".format(np.percentile(tau[~tau.mask[:,0],0],25)),
            'quality 3mm':"{0:0.3f}".format(median(qtau3[~qtau3.mask])),
            'quality 2mm':"{0:0.3f}".format(median(qtau2[~qtau2.mask])),
            'height':heights[mkey],"start":times[0],'end':times[1],'coord':coord[mkey]
            
                   } #подсчет йвартилей, средних значение и отношения ошибоу вписования к величине.
        taus[mkey]=tau
        time[mkey]=times
        
        
        import datetime
        in_name = f

        meteotime = datetime.datetime.strptime(in_name[-in_name[::-1].index('/'):in_name.index('_tau')],'%Y-%m-%d_%H-%M-%S')
        mname = meteotime.strftime("%Y%m%d")
        import glob
        met = glob.glob(in_name[:-in_name[::-1].index('/')]+mname+"*.txt")
        if len(met) ==0:
            print("No meteo data found! (and it is OK)")
            continue
        if len(met) > 1:
            raise Warning(f"Multiple meteo files found: {meteo}")
        met = met[0]
        import pandas as pd
        fr = pd.read_csv(open(met,encoding="CP1251"),' ',parse_dates={"time":[0,1]},header=0,skipinitialspace=True,dayfirst=True)
        
        cols = ['time','T','P','Hu']
        if mkey in meteo:
            for n in cols:
                meteo[mkey][n] = np.concatenate(meteo[mkey][n], fr[n].to_numpy(),axis=0)
        else:
            meteo[mkey] = {}
            for n in cols:
                meteo[mkey][n] = fr[n].to_numpy()

        for c in cols[1:]:
            stat[mkey]["Median "+c] = "{0:0.3f}".format(median(meteo[mkey][c]))
        


        
    
    fr = pd.DataFrame(stat)
    
    fr.to_excel('report.xls') #пихнем в эксель, чтобы читать удобнее было.
    

    def legend_without_duplicate_labels(ax):
        handles, labels = ax.get_legend_handles_labels()
        unique = [(h, l) for i, (h, l) in enumerate(zip(handles, labels)) if l not in labels[:i]]
        ax.legend(*zip(*unique))


    figure()
    ax = gca()
    for key in taus:
        mkey = key[:3]
        for k in heights:
            if mkey in k:
                mkey = k
                break
        tau = taus[key]
        #ax.errorbar(tau[:,0],tau[:,1],tau[:,3],tau[:,2],markers[mkey],colors[mkey],alpha=0.005,ms=1)
        ax.scatter(tau[:,0],tau[:,1],marker=markers[mkey],c=colors[mkey],label=mkey,s=size[mkey],alpha=1,zorder=zorder[mkey])
    xlabel(r"$\tau$, 3mm")
    ylabel(r"$\tau$, 2mm")
    xlim(0,0.5)
    ylim(0,0.8)
    legend_without_duplicate_labels(ax)
    
    savefig("all_tau.png")
    savefig("all_tau.pdf")
    
    figure()
    
    for key in ['Гуниб14.10.2','Маяк_30.10.2','ГБС_30.10.2']:
        mkey = key[:3]
        for k in heights:
            if mkey in k:
                mkey = k
                break
    
        plot(time[key],taus[key][:,0],'.',marker=markers[mkey],c=colors[mkey],label=mkey+", 3mm",markersize=size[mkey]/10,alpha=1,zorder=zorder[mkey])
        plot(time[key],taus[key][:,1],'--',marker=markers[mkey],c=colors[mkey],label=mkey+", 2mm",markersize=size[mkey]/10,alpha=1,zorder=zorder[mkey])
    xlim(19294.815740442056, 19295.94522408901)
    ylim(0.0, 0.25)
    import matplotlib.dates as mdates
    locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
    formatter = mdates.ConciseDateFormatter(locator)
    ax = gca()
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)
    legend()
    savefig("step_tau.png")
    savefig("step_tau.pdf")

    figure()
    
    ax = gca()
    for key in taus:
        mkey = key[:3]
        for k in heights:
            if mkey in k:
                mkey = k
                break
        tau = taus[key]
        ax.scatter(stat[key]['height'],stat[key]['3mm'],marker=markers[mkey],c=colors[mkey],label=mkey+", 3mm",s=size[mkey],alpha=1,zorder=zorder[mkey])
        ax.scatter(stat[key]['height'],stat[key]['2mm mean'],marker=markers[mkey],c=colors[mkey],label=mkey+", 2mm",s=size[mkey]*2,alpha=1,zorder=zorder[mkey])
    xlabel('Высота, м')
    ylabel('Оптическая толщина')
    legend()
    #ax.yaxis.set_ticks(linspace(0.066,0.170,4))
    savefig("tau_height_mean.png")
    savefig("tau_height_mean.pdf")
        
    figure()
    
    ax = gca()
    for key in taus:
        mkey = key[:3]
        for k in heights:
            if mkey in k:
                mkey = k
                break
        tau = taus[key]
        ax.scatter(stat[key]['height'],stat[key]['q1:3mm'],marker=markers[mkey],c=colors[mkey],label=mkey+", 3mm",s=size[mkey],alpha=1,zorder=zorder[mkey])
        ax.scatter(stat[key]['height'],stat[key]['q1:2mm'],marker=markers[mkey],c=colors[mkey],label=mkey+", 2mm",s=size[mkey],alpha=1,zorder=zorder[mkey])
    xlabel('Высота, м')
    ylabel('Оптическая толщина')
    legend()
    #ax.yaxis.set_ticks(linspace(0.066,0.170,4))
    savefig("tau_height_q1.png")
    savefig("tau_height_q1.pdf")
    
    import matplotlib.dates as mdates

    for key in meteo:
        mkey = key[:3]
        for k in heights:
            if mkey in k:
                mkey = k
                break
        tau = taus[mkey]
        ti = np.array(time[mkey])
        mti = meteo[mkey]['time']
        
        mti = mti-min(ti)
        ti = ti-min(ti)

        mti=mti/np.timedelta64(10, "s")
        ti=ti/np.timedelta64(10, "s")
        
        from scipy.interpolate import interp1d
        for m in cols[1:]:
            plot(mti,meteo[mkey][m])
            f = interp1d(mti, meteo[mkey][m],bounds_error=False,fill_value=-1)
            meteo[mkey][m] = ma.array(f(ti))
            meteo[mkey][m][meteo[mkey][m]==-1] = ma.masked
        meteo[mkey].pop('time')
            
        #ax.errorbar(tau[:,0],tau[:,1],tau[:,3],tau[:,2],markers[mkey],colors[mkey],alpha=0.005,ms=1)

    figure()
    for key in meteo:
        i=0
        for m in cols[1:]:
            i+=1
            subplot(3,1,i)
            plot(time[mkey],meteo[mkey][m],'-c')
            ylabel(m)
            twinx()
            plot(time[mkey],taus[mkey][:,0],'-b')
            ylabel(r"$\tau$, 3mm")
            ylim(0.05,0.5)
            locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
            formatter = mdates.ConciseDateFormatter(locator)
            ax = gca()
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            ax.set_yscale('log')
    

    figure()
    for key in meteo:
        i=0
        for m in cols[1:]:
            i+=1
            subplot(3,2,2*i)
            scatter(taus[mkey][:,0],meteo[mkey][m],s=2)
            ylabel(m)
            xlabel(r"$\tau$, 3mm")
            xlim(0,.4)
    savefig("meteo.png")
            
    for key in meteo:
        i=0
        for m in cols[1:]:
            i+=1
            subplot(3,2,2*i-1)
            scatter(taus[mkey][:,1],meteo[mkey][m],s=2)
            ylabel(m)
            xlabel(r"$\tau$, 2mm")
            xlim(0,.4)
    savefig('scatters.png')
    
    X = array([meteo[mkey]['T'],meteo[mkey]['P'],meteo[mkey]['Hu']]).swapaxes(0,1)
    from sklearn.preprocessing import StandardScaler, MinMaxScaler, FunctionTransformer
    from sklearn.decomposition import PCA, KernelPCA, SparsePCA, FastICA, LatentDirichletAllocation
    from sklearn.pipeline import Pipeline
    
    from sklearn.linear_model import LinearRegression, TheilSenRegressor
    from sklearn.linear_model import RANSACRegressor

    pca = PCA(n_components=3, svd_solver='full')
    scaler = StandardScaler()
    scaler.fit(X)
    pipe = Pipeline([('scaler',scaler),('Dim reduction and decorrelator',pca)])
    pca.fit(scaler.transform(X))
    
    
    estimators = [
        ("RANSAC", RANSACRegressor(random_state=42)),
    ]
    colors = {"OLS": "turquoise", "Theil-Sen": "gold", "RANSAC": "lightgreen"}
    lw = 2
    
    figure()
    scatter(pipe.transform(X)[:,0],tau[:,0],s=1,label='PC1')    
    scatter(pipe.transform(X)[:,1],tau[:,0],s=1,label='PC2')    
    scatter(pipe.transform(X)[:,2],tau[:,0],s=1,label='PC3')   
    line_x = np.array([-3, 3])
    
    for name, estimator in estimators:
        estimator.fit(pipe.transform(X)[:,2][~tau[:,0].mask][:,None], tau[:,0][~tau[:,0].mask].filled())
        y_pred = estimator.predict(line_x.reshape(2, 1))
        plt.plot(
            line_x,
            y_pred,
            color=colors[name],
            linewidth=lw,
            label="%s fit" % (name),
        )


    ylabel(r"$\tau$, 3mm")
    legend()
    ylim(0,.4)
    xlabel("PC amplitude")
    savefig("PCA.png")



