import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import timedelta
from scipy import interpolate 
from sklearn.linear_model import LinearRegression, HuberRegressor
from sklearn.decomposition import PCA, FastICA, FactorAnalysis
#from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split



import netCDF4 as nc
from datetime import timedelta

from scipy import interpolate 
from sklearn.linear_model import LinearRegression, HuberRegressor 


from keras.layers import LSTM, Dense, Conv1D, MaxPooling1D, TimeDistributed
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing

from sklearn.decomposition import PCA, FastICA, FactorAnalysis
from sklearn.neighbors import KNeighborsRegressor 
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error ,mean_squared_error, median_absolute_error,confusion_matrix,accuracy_score,r2_score
import sklearn.metrics as metrics
from sklearn.preprocessing import MinMaxScaler

def hampel(data, window_size, simg=3):    
    n = len(data)
    new_data = data.copy()
    k = 1.4826
    idx = []

    for i in range((window_size),(n - window_size)):
        r_median = np.median(data[(i - window_size):(i + window_size)]) 
        r_mad  = k * np.median(np.abs(data[(i - window_size):(i + window_size)] - r_median)) 
        
        if (np.abs(data[i] - r_median) > simg * r_mad):
            new_data[i] = r_median #замена выброса
            idx.append(i)

    return new_data, idx

def load(in_name = 'MIAP-2 data/01_Badary',i=0): 
    import datetime 
     
    uc = np.array([10,11,12,13,14,15,16,17,18,19,20,21]) 
    uc2 = uc - 2 
         
    year = 2016 + i%12 
    tdat = [] 
    i = 0 
    for l in open(in_name+'.dat',encoding='CP1251').readlines():
        i+=1 
        tt = l.split('\t')[6] 
        td = tt.split(' ')         
        while '' in td:  
            td.remove('') 
        try: 
            tdat.append(datetime.datetime.strptime(tt,'%d.%m.%Y %H:%M:%S')) 
        except: 
            try: 
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[2]),int(td[3]),int(td[4]))) 
            except: 
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[1]),int(td[2]),int(td[3]))) 
     
            uc2=uc        
     
    data2 = np.loadtxt(in_name+'.dat',usecols=uc2) 
    return(tdat,data2)
    
def interp(data, t_old, t_new, t0):
    try:
        f = interpolate.interp1d((t_old-t0)/np.timedelta64(1,'s'), data,kind='linear',bounds_error=False, fill_value='extrapolate')
    except:
        f = interpolate.interp1d((t_old-t0)/np.timedelta64(1,'s'), data.swapaxes(0,1), kind='linear',bounds_error=False, fill_value='extrapolate')
    data_new = f((t_new-t0)/np.timedelta64(1,'s')) 
    data_new = np.ma.array(data_new,mask=np.isnan(data_new))
    data_new[np.isnan(data_new)] = 0
    data_new[data_new==0]=np.ma.masked
    return data_new

def plot_time_series(time_data):
    plt.figure(figsize=(13,8))
    for i, values in enumerate(time_data):
        begin_data = []
        begin_data.append(time_data[i][0])
        if len(time_data[i]) == 4:
            plt.plot(time_data[i][0], time_data[i][1], label=time_data[i][2], linewidth=time_data[i][3])
        else:
            plt.plot(time_data[i][0], time_data[i][1], label=time_data[i][2], linewidth=2)
    plt.xlabel('Time', size = 14)
    plt.ylabel('PWV, cm', size = 14)
    plt.ylim(-1,5)
    plt.legend()
    plt.show()
    
def R_squared(X1,X2):
    lr = LinearRegression()
    try: 
        lr.fit(X1, X2)
        R_2 = lr.score(X1, X2)
    except:
        lr.fit(X1.reshape(-1, 1), X2)
        R_2 = lr.score(X1.reshape(-1, 1), X2)
    return R_2

def scatt_R(x, y, labelx, labely, save_path):
    try:
        z = np.polyfit(x, y, 1)
        p = np.poly1d(z)
        xx = x[x.argsort()]
    except:
        try:
            z = np.polyfit(x, y.ravel(), 1)
            p = np.poly1d(z)
            xx = x[x.argsort()]
        except:
            z = np.polyfit(x.ravel(), y, 1)
            p = np.poly1d(z)
            xx = y[y.argsort()]

    plt.figure(figsize=(13, 8))
    plt.xlabel('{}'.format(labelx), size=20)
    plt.ylabel('{}'.format(labely), size=20)
    plt.plot(xx, p(xx), color='k', linestyle='-.', linewidth=2, label=('{var} = {value}'.format(var='$R^2$', value='%.2f' % R_squared(x, y))))
    plt.scatter(x, y, s=3, marker='x', alpha=0.25)
    plt.legend(fontsize=18)
    plt.gca().tick_params(axis='both', which='major', labelsize=18)
    #plt.rc('font', size= 12)
    try:
        plt.savefig(save_path)
        print("Рисунок успешно сохранен.")
    except:
        plt.show()
    
def scatt_trend(x, y, labelx, labely):
    
    try:
        z = np.polyfit (x, y, 1)
        p = np.poly1d (z)
        xx = x[x.argsort()]
    except:
        try:
            z = np.polyfit (x, y.ravel(), 1)
            p = np.poly1d (z)
            xx = x[x.argsort()]
        except:
            z = np.polyfit (x.ravel(), y, 1)
            p = np.poly1d (z)  
            xx = y[y.argsort()]
    
    plt.figure(figsize=(10,10))
    plt.xlabel('{}'.format(labelx), size = 24)
    plt.ylabel('{}'.format(labely), size = 24)
    plt.scatter(x, y, s=4, marker='x',alpha=0.3)
    plt.plot (xx, p(xx), color = 'k', linestyle='-.', linewidth=2, label=('{var} = {value}'.format(var='$R^2$', value= '%.2f' %  R_squared(x, y))))
    plt.semilogx ()
    plt.semilogy ()
    plt.xlim(10**(-1))
    plt.ylim(10**(-1))
    plt.legend(fontsize=24)
    plt.gca().tick_params(axis='both', which='major', labelsize=22)
    plt.tight_layout(pad=0.1, w_pad=0.5, h_pad=1.0)
    plt.savefig('scatt_pred.pdf')
    #plt.show()
    
from sklearn.decomposition import PCA, FastICA, FactorAnalysis

def pca_analize(data):
    plt.figure(figsize=(8,5))
    pca = PCA()
    pca.fit_transform(data)
    pca_variance = pca.explained_variance_
    plt.plot(pca_variance)
    plt.ylabel('Variance ratio')
    plt.xlabel('Principal components')
    
def pca_def (data_miap, data_nasa):
    pca = PCA(n_components=8)
    ica = FastICA(n_components=8)
    fa = FactorAnalysis()
    scores_regr = {}
    dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
    for esn in dimr:
        es = dimr[esn]
        es.fit(data_miap)
        pc = es.transform(data_miap)
        for iPC in range(pc.shape[1]):
            estimator = HuberRegressor(max_iter=10000)
            Xr = pc[:,iPC][np.logical_not(data_nasa.mask),None]
            yr = data_nasa[np.logical_not(data_nasa.mask)]
            estimator.fit(Xr, yr)
            scor = estimator.score(Xr, yr)
            scores_regr[esn+"{}".format(iPC)]=scor
            print("Score:",scor," estimator:",esn,' C:',iPC)
    
    pc = ica.transform(data_miap)
    return(pc, ica) 


def pca_comp (data_miap, data_nasa, n_components):
    pca = PCA(n_components=n_components)
    ica = FastICA(n_components=n_components)
    fa = FactorAnalysis()
    scores_regr = {}
    dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
    for esn in dimr:
        es = dimr[esn]
        es.fit(data_miap)
        pc = es.transform(data_miap)
        for iPC in range(pc.shape[1]):
            estimator = HuberRegressor(max_iter=10000)
            Xr = pc[:,iPC][np.logical_not(data_nasa.mask),None]
            yr = data_nasa[np.logical_not(data_nasa.mask)]
            estimator.fit(Xr, yr)
            scor = estimator.score(Xr, yr)
            scores_regr[esn+"{}".format(iPC)]=scor
            #print("Score:",scor," estimator:",esn,' C:',iPC)
    
    pc = ica.transform(data_miap)
    return(pc, ica) 
    

def round_time(data, min_round):
    
    t = np.zeros(len(data)).astype(str)
    try: 
        data[0].minute
    except:
        data = pd.to_datetime(data)
    for i in range(len(data)):
        if (data[i].minute * 60 + data[i].second) % (min_round*60) != 0:
            r = round ((data[i].minute * 60 + data[i].second) / (min_round*60))
            s = (r*min_round*60 - (data[i].minute * 60 + data[i].second))
            t[i] = data[i] + timedelta(seconds=s)
        else:
            t[i] = data[i]
    t = np.array(t, dtype='datetime64[s]')
    return t

def new_time(data, minute):
    minute = minute
    delta = np.timedelta64('{}'.format(60*minute), 's')
    new_t = []
    i = 1
    new_t.append(data[0])
    while (new_t [-1] < data[-1]):
        new_t.append(data[0]+i*delta)
        i += 1
    td = pd.to_datetime(new_t)
    td = td.round("min")
    return td

def model_shape3(X_transform,y_transform,i, xscaler, yscaler):
    from tensorflow import keras
    from keras.layers import LSTM, Dense, Conv1D, MaxPooling1D, TimeDistributed
    from tensorflow import keras
    from tensorflow.keras import layers
    
    X_transform = X_transform.reshape(X_transform.shape[0],1, X_transform.shape[1])
    X_train, X_test, Y_train, Y_test = train_test_split(X_transform, y_transform, test_size=0.25)
    ss = np.array(X_transform.shape[1:])
    input_shape = X_transform.shape
    model = keras.Sequential()
    model.build(input_shape)
    model.add(Dense((i+1)*ss.prod(), activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
    model.add(LSTM(200, activation='relu', return_sequences =True)) # рекурентная нейронная сеть с долгосрочной памятью (обрабатывает данные в виде shape = (xx, xx, xx))
    model.add(layers.Flatten(input_shape=(ss))) # сглаженный слой (объединяет даные в одномерный вектор)
    model.add(Dense((i+1)*ss.prod()**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой/ можно убрать 
    model.add(Dense((i+1)*ss[1]**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
    model.add(Dense(ss[1], activation='softmax'))
    model.add(Dense(1)) # выходной слой
    model.compile(optimizer='adam', loss='mean_absolute_error')
    #model.summary()
    
    logdir=f"logs/fit/{i}/"
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    history = model.fit(X_train, Y_train, epochs=300, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])
    
    from keras.utils import plot_model
    plot_model(model, to_file='model.png', show_shapes=True)
    y_pred_test = model.predict(X_test)[:,0]
    y_pred_train = model.predict(X_train)[:,0]  
    #смотрю на регрессию между предсказанными данными и данными merra
    x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
    y_scaler = yscaler.inverse_transform(y_pred_test[:, None])  
    lr = LinearRegression()
    lr.fit(x_scaler.reshape(-1, 1), y_scaler)
    r_squared = lr.score(x_scaler.reshape(-1, 1), y_scaler)
    print('r^2 = ', r_squared) 
    return y_pred_test, y_pred_train, history
    
def ann_fit(X_,y_transform,yscaler,i, path, input_shape, ss):
    from tensorflow import keras
    from keras.layers import LSTM, Dense, Conv1D, MaxPooling1D, TimeDistributed
    from tensorflow import keras
    from tensorflow.keras import layers
    
    X_train, X_test, Y_train, Y_test = train_test_split(X_, y_transform, test_size=0.25)
    #input_shape = X_.shape
    model = keras.Sequential()
    model.build(input_shape)
    model.add(Dense((i+1)*ss.prod(), activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros'))
    model.add(LSTM(200, activation='relu', return_sequences =True)) # рекурентная нейронная сеть с долгосрочной памятью (обрабатывает данные в виде shape = (xx, xx, xx))
    model.add(layers.Flatten(input_shape=(ss))) # сглаженный слой (объединяет даные в одномерный вектор)
    model.add(Dense((i+1)*ss.prod()**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой/ можно убрать 
    model.add(Dense((i+1)*ss[1]**2, activation='relu', kernel_initializer='random_uniform', bias_initializer='zeros')) # плотный полностью связный слой
    model.add(Dense(ss[1], activation='softmax'))
    model.add(Dense(1)) # выходной слой
    model.compile(optimizer='adam', loss='mean_absolute_error')
    #model.summary()
    
    logdir=f"logs/fit/{i}/"
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    history = model.fit(X_train, Y_train, epochs=64, validation_data=(X_test, Y_test), callbacks=[tensorboard_callback])
    
    from keras.utils import plot_model
    plot_model(model, to_file='model.png', show_shapes=True)
    y_pred_test = model.predict(X_test)[:,0]
    y_pred_train = model.predict(X_train)[:,0]  
    #смотрю на регрессию между предсказанными данными и данными merra
    x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
    y_scaler = yscaler.inverse_transform(y_pred_test[:, None])  
    lr = LinearRegression()
    lr.fit(x_scaler.reshape(-1, 1), y_scaler)
    r_squared = lr.score(x_scaler.reshape(-1, 1), y_scaler)
    print('r^2 = ', r_squared) 
    #рисуноки предсказанного значения и MERRA
    scatt_R(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm', path)
    
    return y_pred_test, y_pred_train, history, model

def time_frame(t_merra, t_miap):
    for i in range(len(t_merra)):
        if t_merra[i].year == t_miap[len(t_miap)-1].year and t_merra[i].month == t_miap[len(t_miap)-1].month and t_merra[i].day == t_miap[len(t_miap)-1].day and t_merra[i].hour == t_miap[len(t_miap)-1].hour:
            end = i
            
    for i in range(len(t_merra)):
        if t_merra[i].year == t_miap[0].year and t_merra[i].month == t_miap[0].month and t_merra[i].day == t_miap[0].day and t_merra[i].hour == t_miap[0].hour:
            start = i
    print(' конец наблюдения time merra', t_merra[end], 'конец наблюдения time miap', t_miap[len(t_miap)-1],'\n', 
          'начало наблюдения time merra', t_merra[start], 'начало наблюдения time miap', t_miap[0])
    return start, end

def same_time(t_merra,t_miap):
    ind_merra = []
    ind_miap = []
    j = 0
    k = 0
    for i in range(len(t_merra)):
        if len(ind_merra) == 0:
            k = 0
        else:
            k = ind_miap[-1]
        for j in range(k, len(t_miap)):
            
            if t_merra[i].year == t_miap[j].year and t_merra[i].month == t_miap[j].month  and t_merra[i].day == t_miap[j].day and t_merra[i].hour == t_miap[j].hour and t_merra[i].hour == t_miap[j].hour:
                #print(t_merra[i], t_miap[j])
                ind_merra.append(i)
                ind_miap.append(j)
                break
    return ind_merra, ind_miap

def data_same_time(X, ind_miap):
    u = np.array(X[ind_miap[0]:ind_miap[1]])
    u = u.reshape(1, (ind_miap[1] - ind_miap[0]), X.shape[1])
    for i in range(1, len(ind_miap)-1):
        Y = X[ind_miap[i]:ind_miap[i+1]].reshape(1, (ind_miap[1] -ind_miap[0]), X.shape[1])
        u = np.append(u,Y, axis = 0)
    return u
    
def new_data(time_miap, data):
    t = time_miap
    mask = np.isnan(time_miap)
    ind_tt, dif_tt, difdats, median = diff_time(t)

    count = 0 
    for i in range(len(ind_tt)):
        if difdats[ind_tt[i]] != median:
            if difdats[ind_tt[i]]//np.timedelta64(600, 's') == 2 and difdats[ind_tt[i]-1]//np.timedelta64(600, 's') != 0:
                data = np.insert(data, ind_tt[i]+1+count, (data.swapaxes(0,1)[ind_tt[i]+count+1]+data.swapaxes(0,1)[ind_tt[i]+count-1])/2, axis=1)
                t = np.insert (t, ind_tt[i]+1+count, t[ind_tt[i]+count]+np.timedelta64('{}'.format(60*10), 's'))
                mask = np.insert (mask, ind_tt[i]+1+count, True)
                count += 1
            elif difdats[ind_tt[i]]//np.timedelta64(600, 's') == 2 and difdats[ind_tt[i]-1]//np.timedelta64(600, 's') == 0:
                t[ind_tt[i]+1+count] = t[ind_tt[i]+count]+np.timedelta64('{}'.format(60*10), 's')
                count += 1
                
    ind_tt, dif_tt, difdats, median = diff_time(t)
    count = 0 
    for i in range(len(ind_tt)):
        if dif_tt[i] //np.timedelta64(600, 's') != 0:
            L = [np.nan] * (int(dif_tt[i]/np.timedelta64(600, 's') )-1)
            for j in range(len(L)): 
                L[j] = t[ind_tt[i]+count] + (j+1)*np.timedelta64('{}'.format(60*10), 's')
            t = np.insert (t, ind_tt[i]+1+count, L)
            for k in range(len(L)): 
                data = np.insert(data, ind_tt[i]+1+count + k, 0, axis=1)
                mask = np.insert (mask, ind_tt[i]+1+count, True)
            count += len(L)

    count = 0
    ind_tt, dif_tt, difdats, median = diff_time(t)
    for i in range(len(ind_tt)):
        if dif_tt[i] //np.timedelta64(600, 's') == 0:
            data = np.delete(data, ind_tt[i]-count, axis=1)
            t = np.delete(t, ind_tt[i]-count)
            mask = np.delete(mask, ind_tt[i]-count)
            count +=1
    return data, t, mask
   
def diff_time(time):
    ind_tt = []    
    dif_tt = []    
    difdats = np.diff(time) 
    median = np.median(difdats) 
    for i in range(len(difdats)):    
        if difdats[i] != median: 
            ind_tt.append(i)    
            dif_tt.append(difdats[i])
    return ind_tt, dif_tt, difdats, median
    
def standardize_data(data):
    min_val = np.min(data)
    max_val = np.max(data)
    standardized_data = (data - min_val) / (max_val - min_val) * 2 - 1
    return standardized_data, min_val, max_val

def restore_data(standardized_data, min_val, max_val):
    restored_data = (standardized_data + 1) / 2 * (max_val - min_val) + min_val
    return restored_data

def knn_pred(x_train, x_test, y_train,y_test):
    knn_regr = KNeighborsRegressor(n_neighbors=3)
    knn_regr.fit(x_train,y_train)
    y_pred_test = knn_regr.predict(x_test)
    y_pred_train = knn_regr.predict(x_train)
    return y_pred_test, y_pred_train

def R_score_for_knn(data, merra, yscaler, place, k, path):
    r_squared = []
    for i in range(k):
        X_train, X_test, Y_train, Y_test = train_test_split(data, merra, test_size=0.25)
        y_pred_test, y_pred_train = knn_pred(X_train, X_test, Y_train, Y_test)
        r_squared.append(R_squared(Y_test, y_pred_test))
    print('R^2 для тестовой выборки и предсказанной ({}) = '.format(place), np.sum(r_squared)/k)
    x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
    y_scaler = yscaler.inverse_transform(y_pred_test[:, None]) 
    scatt_R(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm', '{}/{}_knn.jpg'.format(path, path[12:]))
    scatt_trend(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm')
    return r_squared
    
def R_score_knn_fit(data, merra, yscaler, knn_fit, place, k, path):
    r_squared = []
    for i in range(k):
        X_train, X_test, Y_train, Y_test = train_test_split(data, merra, test_size=0.25)
        y_pred_test = knn_fit.predict(X_test)
        y_pred_train = knn_fit.predict(X_train)
        r_squared.append(R_squared(Y_test, y_pred_test))
    print('R^2 для тестовой выборки и предсказанной ({}) = '.format(place), np.sum(r_squared)/k)
    x_scaler = yscaler.inverse_transform(Y_test[:, None])[:,0]
    y_scaler = yscaler.inverse_transform(y_pred_test[:, None]) 
    scatt_R(x_scaler, y_scaler, 'MERRA-2, cm', 'Prediction, cm', '{}/{}_knn.jpg'.format(path, path[21:]))
    return r_squared

def data_preparation(t_merra, t_miap, data, water_merra, mask):
    ind_merra, ind_miap = same_time(t_merra,t_miap)
    u = data_same_time(data, ind_miap)
    u_new = u.reshape(u.shape[0], u.shape[1]*u.shape[2])
    
    mask = mask[ind_miap[:-1]]
    t_merra = t_merra[:-1]
    water_merra = water_merra[:-1]
    u_new = u_new[np.logical_not(mask)]
    water_merra = water_merra[np.logical_not(mask)]
    mask = mask[np.logical_not(mask)]
    
    return u_new, water_merra, mask
    

