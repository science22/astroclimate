#!/usr/bin/python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 19 16:46:40 2018

Optical depth processing script for MIAP-2

Usage: python sniplet.py [path to .dat]
noise.dat is readed from cwd


@author: pzemlyan, mailto:petez@ipfran.ru
"""
from statsmodels.robust import mad as stand_mad
from scipy import stats
import numpy as np
from numpy.linalg import norm
from numpy import array,loadtxt,zeros,savetxt,cos,linspace,exp
from numpy import ma

from scipy.constants import c,h,k,pi
import numpy as np
from math import log, atan, sin, sqrt
from enum import Enum
from astropy import units as u;
from lmfit import minimize, Parameters, Parameter, report_fit
import pywt


import matplotlib.dates as mdates

from matplotlib.pyplot import figure,subplot,legend,plot,savefig,ylabel,xlabel, gca, fill_between, ylim, title


###
#change me
#in_name = '2016-07-01_13-25-44'###

def process_dat_file(in_name,images):
    '''
    Функция процессинга одного .dat файла
    '''
    print("processing "+in_name)
    tdats = []
    taus = []

    import datetime
    
    #чтение данных
    
    uc=array((10,11,12,13,14,15,16,17,18,19,20,21)) #выбор колонок для чтения
    uc2 = uc-2 #он менялся
    tdat = []
    
    lines = open(in_name+'.dat',encoding='CP1251').readlines()
    for l in lines[45:]:
        tt = l.split('\t')[6]
        td = tt.split(' ')
        while '' in td: 
            td.remove('')
        try:
            tdat.append(datetime.datetime.strptime(tt,'%d.%m.%Y %H:%M:%S'))
        except:
            try:
                tdat.append(datetime.datetime(2016,1,int(td[0]),int(td[2]),int(td[3]),int(td[4]))) #формат даты менялся - не можем прочитать один - читаем другой
            except:
                tdat.append(datetime.datetime(2016,1,int(td[0]),int(td[1]),int(td[2]),int(td[3])))
    
            uc2=uc
    
            
    import os
    #куда сваливать обработанные файлы
    if not os.path.isdir('processed'):
        os.mkdir('processed')    
    
    
    
    
    #измерения собственных шумов МИАП, на самом деле не нужны. Чуть ниже используется вэйвлет фильтр, данные используются для его проверки.
    data = loadtxt('noise.dat',usecols=uc)
    tnoise = loadtxt('noise.dat',usecols=(6,7,8,9))
    data2 = loadtxt(in_name+'.dat',usecols=uc2,skiprows=45,encoding='CP1251')

    print(f"contains {data2.shape[0]} records")
    if data2.shape[0]<3 or data2.ndim < 2:
        return

    
    ##читаем углы
    ang = []
    for i in range(6):
        l = lines[i+1]
        ang.append(float(l[l.index('=')+1:l.index('\n')])*2*pi/360)
    
    
    #в этот массив сложены временные отсчеты.
    tdat = array(tdat)#,dtype='datetime64')
    noise3mm_data = data[:,0]
    noise2mm_data = data[:,6]
    
    timed = []
    for t in tdat:
        timed.append(t.timestamp())
    timed = array(timed)
    
    #taudat = loadtxt(in_name+'.dat',usecols=(1,4),skiprows=45,encoding='CP1251')
    filtered = ma.zeros([data2.shape[0],13])
    if data2.shape[0]<20:
        print('Time series is too short')
        filtered[:,0]=timed
        filtered[:,1:] = data2
        filtered.mask = zeros(filtered.shape)!=0

        if images: #рисуем график с результатом.

            figure()
            subplot(211)
            for i in range(6):
            #    plot(timed,mask[:-1,i])
                plot(tdat,data2[:,i],'-c')
            legend(['raw data','filtered,masked'])
            ylabel('V,Voltage')
            ax=gca()
            locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            
            subplot(212)
            for i in range(6,12):
                plot(tdat,data2[:,i],color='#a05050')
            legend(['raw data','filtered,masked'])
            ax=gca()
            locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            
            ylabel('V,Voltage')
            xlabel('time, days')

    else:
        
    
        
        #Подсчет коэффициентов фильтра высоких частот с помошью вэйвлетов
        #Что эквиволентно скользящемо окну с взвешенным усреднением
        
        
        timen = tnoise[:,0]+tnoise[:,1]/24+tnoise[:,2]/24/60+tnoise[:,3]/24/60/60
        noisy_coefs = pywt.wavedec(noise3mm_data, 'coif2', mode='per')
        
        sigma = stand_mad(noisy_coefs[-1])
        uthresh = 2*sigma*np.sqrt(2*np.log(len(noise3mm_data)))
        
        denoised = noisy_coefs[:]
        denoised[1:] = (pywt.threshold(i,uthresh) for i in denoised[1:])
        
        signal_3mm = pywt.waverec(denoised, 'coif2', mode='per')
        
        th_3mm = uthresh/len(noise3mm_data)*data2.shape[0]*0.7
        
        noisy_coefs = pywt.wavedec(noise2mm_data, 'coif2', mode='per')
        sigma = stand_mad(noisy_coefs[-1])
        uthresh = sigma*np.sqrt(2*np.log(len(noise2mm_data)))
        
        denoised = noisy_coefs[:]
        denoised[1:] = (pywt.threshold(i,uthresh) for i in denoised[1:])
        
        signal_2mm = pywt.waverec(denoised, 'coif2', mode='per')
        
        th_2mm =uthresh
    
        #можно и нарисовать, но зачем?
        '''
        figure()
        plot(timen,noise3mm_data,'-c')
        plot(timen,signal_3mm,'-b')
        savefig('noise3mm.pdf')
        
        figure()
        plot(timen,noise2mm_data,'-c')
        plot(timen,signal_2mm,'-b')
        savefig('noise2mm.pdf')
        '''
        
        #в этот массив будем складывать обработанные данные
        filtered = zeros([data2.shape[0],13])
        filtered[:,0]=timed
        flag = False
        if len(filtered)%2==1: #при вэйвлет преобразовании важна четность
            flag = True
            filtered = zeros([data2.shape[0]+1,13])
            filtered[:-1,0]=timed
        
        for i in range(6): #сглаживание 3мм канала
            noisy_coefs = pywt.wavedec(data2[:,i], 'coif2', mode='per')
            denoised = noisy_coefs[:]
            denoised[1:] = (pywt.threshold(i,th_3mm) for i in denoised[1:])
            filtered[:,i+1] = pywt.waverec(denoised, 'coif2', mode='per')
        
        for i in range(6,12): #сглаживание 2мм канала
            noisy_coefs = pywt.wavedec(data2[:,i], 'coif2', mode='per')
            denoised = noisy_coefs[:]
            denoised[1:] = (pywt.threshold(i,th_2mm/2) for i in denoised[1:])
            filtered[:,i+1] = pywt.waverec(denoised, 'coif2', mode='per')
        if flag:
            filtered=filtered[:-1]
    
        if images:     #отрисовка результатов сглаживания, для каждого угла
            figure()
            title(in_name)
            subplot(211)
            for i in range(6):
                plot(tdat,data2[:,i],'-c')
                plot(tdat,filtered[:,i+1],'-b')
            legend(['raw data','filtered'])
            ylabel('V,Voltage')
            ax=gca()
            locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            
            subplot(212)
            for i in range(6,12):
                plot(tdat,data2[:,i],color='#a05050')
                plot(tdat,filtered[:,i+1],color='#ff0000')
            
            ax=gca()
            locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            
            legend(['raw data','filtered'])
            ylabel('V,Voltage')
        
        
    
        filtered = zeros([data2.shape[0],13])
        filtered[:,1:]=data2
    
        #сохраним данные после сглаживания
        savetxt('processed/'+in_name+'_filtered.dat',filtered)
        
        #считаем корреляцию между дорожками на разных углах
        #если ее нет - модель плоско-слоистой совсем не работает, углы выкидываем
        
        ccors = []
        for i in range(5):
            x = filtered[:,i+1]
            y = filtered[:,i+2]
            
            chunk = 13
            ccor = []
            for ix in range(len(x)-chunk):
                xc = x[ix:ix+chunk]
                yc = y[ix:ix+chunk]
                if (xc == 0).sum() == chunk or (yc == 0).sum() == chunk:
                    ccor.append(np.nan)
                    continue
                ccor.append(stats.pearsonr(yc/norm(yc),xc/norm(xc))[0])
            ccors.append(array(ccor))
        
        filtered = ma.array(filtered)
        filtered.mask=False
        mask = zeros(filtered[:,1:].shape)!=0
        for i in range(5):
            mask[7:-6,i] = ccors[i]<(1-(1-min(ccors[i]))*0.6) #0.6 - эмпирически. Считаем маску
            
            
        #закрываем углы по маске        
        
        for i in range(4):
            mas = mask[:,i] & mask[:,i+1]
            filtered[mas,i+2] = ma.masked 
            
            
        for i in range(11,6,-1):
            print(i)
            mas = filtered[:,i+1]<filtered[:,i]+abs((filtered[:,i]).max()-filtered[:,i].min())*0.01
            filtered[mas,i] = ma.masked
        
        for i in range(5,0,-1):
            print(i)
            mas = filtered[:,i+1]<filtered[:,i]+abs((filtered[:,i]).max()-filtered[:,i].min())*0.02
            filtered[mas,i] = ma.masked
        
            
        if images: #рисуем график с результатом.
    
            figure()
            subplot(211)
            for i in range(6):
            #    plot(timed,mask[:-1,i])
                plot(tdat,data2[:,i],'-c')
                plot(tdat,filtered[:,i+1],'-b')
            legend(['raw data','filtered,masked'])
            ylabel('V,Voltage')
            ax=gca()
            locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            
            subplot(212)
            for i in range(6,12):
                plot(tdat,data2[:,i],color='#a05050')
                plot(tdat,filtered[:,i+1],color='#ff0000')
            legend(['raw data','filtered,masked'])
            ax=gca()
            locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
            formatter = mdates.ConciseDateFormatter(locator)
            ax.xaxis.set_major_locator(locator)
            ax.xaxis.set_major_formatter(formatter)
            
            ylabel('V,Voltage')
            xlabel('time, days')
        
        #записываем файл с дорожками по углам и временными отсчетами после сглаживания и выкидывания подозрительных углов в файл
        savetxt('processed/'+in_name+'_filtered_masked.dat',filtered)

    '''
    Ниже подсчитывается оптическая толщина.
    Для экспоненты что мы вписываем в "напряжения" по разным углам ничего лучше метода наименьших квадртаов нет.
    Статистические ухищрения что включают машинное обучения применимы для продолжительных наблюдений и в рутинную обработку не входят
    
    Угол 0=0
    Угол 1=60.48
    Угол 2=76.32
    Угол 3=81.36
    Угол 4=84.24
    Угол 5=88.56
    '''
    
    
    
    x =  np.log(1/cos(ang))
    aang = linspace(1.546,0,100)
    base = lambda p,ang: p['Tb'].value*(1-exp(-p['tau'].value/cos(ang)))+p['b'].value
    fit = lambda Tb,tau,b,ang: Tb*(1-exp(-tau/cos(ang)))+b
    
    def resbase(p,y,ang):
        return (y-base(p,ang))
    
    
    p = Parameters()
    p.add('Tb',275.,min=0,max=500, vary=True)
    p.add('tau',0.14,min=0,max=4,vary=True)
    p.add('b',275,min=-500,max=500)
    
    
    tau = ma.zeros((2,len(filtered[:,0])))
    etau = ma.zeros((2,len(filtered[:,0])))

    # в 3мм, по точкам
    for i in range(tau.shape[1]):
        if (filtered[i,1:7].mask).sum()<4:
            ppar = minimize(resbase,p,args=(filtered[i,1:7],ang),method='leastsq')
            tau[0,i] = ppar.params['tau'].value
            etau[0,i] = ppar.params['tau'].stderr
    #        if (etau[0,i]> 3*tau[0,i]): etau[0,i]=ma.masked
        else: 
            tau[0,i]=ma.masked
            etau[0,i]=ma.masked
    
    
    # в 2мм, по точкам
    for i in range(tau.shape[1]):
        if (filtered[i,7:].mask).sum()<4:
            ppar = minimize(resbase,p,args=(filtered[i,7:],ang),method='leastsq')
            tau[1,i] = ppar.params['tau'].value
            etau[1,i] = ppar.params['tau'].stderr
     #       if (etau[1,i]> 3*tau[1,i]): etau[1,i]=ma.masked
        else: 
            tau[1,i]=ma.masked
            etau[1,i]=ma.masked
    
    if images:#рисуем

        figure()
        plot(tdat,tau[0,],'-c',label='3mm')
        fill_between(tdat,tau[0]-etau[0],tau[0]+etau[0],color='c',alpha=0.1)
        plot(tdat,tau[1,],color='#a05050',label='2mm')
        fill_between(tdat,tau[1,]-etau[1],tau[1,]+etau[1],color='#a05050',alpha=0.1)
        ylabel(r'$\tau$')
        legend()
        xlabel('time, days')
        ylim(0,0.5)
        ax=gca()
        locator = mdates.AutoDateLocator(minticks=3, maxticks=7)
        formatter = mdates.ConciseDateFormatter(locator)
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)
    
        savefig('processed/'+in_name+'_tau.pdf')
        savefig('processed/'+in_name+'_tau.png')
    
    tau = tau.filled(np.nan)
    etau = etau.filled(np.nan)
    #dt = np.dtype([('stgr',str),('tau_2mm',float),('tau_3mm',float),('e_2mm',float),('e_3mm',float)])
    
    #сохраняем в том упоротом формате что просил свое время Гриша
    
    out = np.array([(tdat[i].strftime('%d.%m.%Y %H:%M:%S'),tau[0][i],tau[1][i],etau[0][i],etau[1][i],filtered.mask.sum(1)[i]) for i in range(tau.shape[1])])#,dtype=dt)
    h = ''   
    radio = "MIAP:SUFFA" if '0.796' in lines[23] else "MIAP:IPF" #колдунским способом выясняем что за прибор использовался при измерениях. Там были углы разные.
    h+= radio+"\n"
    for l in lines[30:36]:
        h+="#"+l
    savetxt('processed/'+in_name+'_tau_filtered_{0}.dat'.format(radio),out,fmt='%s %s %s %s %s %s',header=h)


def main(args):
    '''
    Основной код программы. Он ищет в указанной папке .dat файлы и пересылает их в функцию процессинга, 
    а потом сводит результаты в одно.
    '''

    import glob
    
    im = False
    
    if len(args) == 0:
        raise ValueError("no args provided")
    
    
    #ищем
    files = glob.glob(args[1]+'/*.dat')
    
    
    for f in files:
        try:
            process_dat_file(f[:-4],im) #обрабатываем
        except BaseException as e:
            print(e) # если что-то случилось, то очень жаль. Но другие файлы все равно постараемся обработать
        
        
    #сведем обработанные файлы и посчитаем статистику по ним.

    
    import pandas as pd
    from numpy import ma, isnan, median
    
    files = glob.glob('processed/'+args[1]+'/*tau*.dat')
    
    stat = {} #словарь со статистикой
    taus = {} #словарь имя файла: массив с оценками оптиеской толщины
    time = {} #словарь имя файла: массив с временными отсчетами
    from dateutil.parser import parse 
    for f in files:
        fl = open(f)
        lines = fl.readlines()
        key=lines[6][12:-4]
        times = []
        for l in lines[8:]:
            times.append(parse(l[:20],dayfirst=True)) #если в одном месте несколько раз наблюдали, соединим в один файл
        
        tau = ma.array(loadtxt(f,usecols=[2,3,4,5]))
        tau[tau==0]=ma.masked
        tau[isnan(tau)]=ma.masked
        if key in taus:
            times = time[key]+times
            tau = np.ma.concatenate((taus[key],tau),axis=0)
            
        qtau3 = (tau[:,2]/tau[:,0])
        qtau2 = (tau[:,3]/tau[:,1])
        stat[key]={
            'source':lines[0],'file':f,
            '2mm mean':"{0:0.3f}".format(tau[:,1].mean()),'3mm':"{0:0.3f}".format(tau[:,0].mean()),'n':tau.shape[0],
            'q1:2mm':"{0:0.3f}".format(np.percentile(tau[~tau.mask[:,1],1],25)),'q1:3mm':"{0:0.3f}".format(np.percentile(tau[~tau.mask[:,0],0],25)),
            'quality 3mm':"{0:0.3f}".format(median(qtau3[~qtau3.mask])),
            'quality 2mm':"{0:0.3f}".format(median(qtau2[~qtau2.mask]))
            
                   } #подсчет йвартилей, средних значение и отношения ошибоу вписования к величине.
        taus[key]=tau
        time[key]=times
        
    
    fr = pd.DataFrame(stat)
    
    fr.to_excel('report.xls') #пихнем в эксель, чтобы читать удобнее было.
    

import sys

if __name__ == "__main__":
    main(sys.argv)

