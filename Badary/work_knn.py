from statsmodels.robust import mad as stand_mad 
from scipy import stats 
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 

import netCDF4 as nc
data = 'g4_areaAvgTimeSeries_M2I1NXINT_5_12_4_TQV_20160101_20171231_102E.nc'
ds = nc.Dataset(data)
water3 = ds['M2I1NXINT_5_12_4_TQV'][:]/10
timed3 = ds['time'][:].data
timed3 = np.array(timed3, dtype='datetime64[s]')

data = 'g4_areaAvgTimeSeries_MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean.nc'
ds = nc.Dataset(data)
water4 = ds['MYD08_D3_6_1_Atmospheric_Water_Vapor_QA_Mean'][:]
timed4 = ds['time'][:].data
timed4 = np.array(timed4, dtype='datetime64[s]')
 
if 'tdats' not in dir(): 
    tdats = [] 
    data = np.zeros((0,12)) 
 
def load(in_name = 'MIAP-2 data/01_Badary',i=0): 
    import datetime 
     
    uc = np.array([10,11,12,13,14,15,16,17,18,19,20,21]) 
    uc2 = uc - 2 
         
    year = 2016 + i%12 
    tdat = [] 
    i = 0 
    for l in open(in_name+'.dat',encoding='CP1251').readlines():
        i+=1 
        tt = l.split('\t')[6] 
        td = tt.split(' ')         
        while '' in td:  
            td.remove('') 
        try: 
            tdat.append(datetime.datetime.strptime(tt,'%d.%m.%Y %H:%M:%S')) 
        except: 
            try: 
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[2]),int(td[3]),int(td[4]))) 
            except: 
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[1]),int(td[2]),int(td[3]))) 
     
            uc2=uc        
     
    data2 = np.loadtxt(in_name+'.dat',usecols=uc2) 
    return(tdat,data2)  

td,dd = load('bdr') 
data = dd 
tdats = np.array(td) 
     
from sklearn.preprocessing import StandardScaler, MinMaxScaler, FunctionTransformer 
from sklearn.decomposition import PCA, KernelPCA, SparsePCA, FastICA, FactorAnalysis 
from sklearn.decomposition import LatentDirichletAllocation 

data=data.swapaxes(0,1) 
data[:6] -= data[:6].mean(axis=0) 
data[6:] -= data[6:].mean(axis=0) 
data=data.swapaxes(0,1)

data_2016 = pd.read_csv('WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
data_2017 = pd.read_csv('WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)

data_2016[data_2016 == data_2016['wt2'][2697]] = np.nan
data_2016[data_2016 == data_2016['wat1'][3630]] = np.nan
data_2017[data_2017 == data_2017['wt2'][3453]]=np.nan
data_2017[data_2017 == data_2017['wat1'][1460]]=np.nan

timed1 = pd.to_datetime(data_2016['date'] + ' ' + data_2016['time']).to_numpy(dtype=np.datetime64)
timed2 = pd.to_datetime(data_2017['date'] + ' ' + data_2017['time']).to_numpy(dtype=np.datetime64)

water_rad1 = data_2016['wt2'].to_numpy(dtype=float)
water_gnss1 = data_2016['wat1'].to_numpy(dtype=float)
water_rad2 = data_2017['wt2'].to_numpy(dtype=float)
water_gnss2 = data_2017['wat1'].to_numpy(dtype=float)

water_rad1[water_rad1 < 0] = 0
water_gnss1[water_gnss1 < 0] = 0
water_rad2[water_rad2 < 0] = 0
water_gnss2[water_gnss2 < 0] = 0

timed_miap = timed1[:13248]
timed_miap = np.append(timed1[:13248], timed2)
water_miap = water_rad1[:13248]
water_miap = np.append(water_rad1[:13248], water_rad2)
water_gnss = water_gnss1[:13248]
water_gnss = np.append(water_gnss1[:13248], water_gnss2)


def hampel(data, window_size, simg=3):    
    n = len(data)
    new_data = data.copy()
    k = 1.4826
    idx = []

    for i in range((window_size),(n - window_size)):
        r_median = np.median(data[(i - window_size):(i + window_size)]) 
        r_mad  = k * np.median(np.abs(data[(i - window_size):(i + window_size)] - r_median)) 
        
        if (np.abs(data[i] - r_median) > simg * r_mad):
            new_data[i] = r_median #замена выброса
            idx.append(i)

    return new_data, idx

water_miap, outliers = hampel(water_miap, 15) # 5 hours
water_gnss, outliers = hampel(water_gnss, 15)
 
tdats = np.array(tdats,dtype=np.datetime64) 

from scipy import interpolate 

f_rrp = interpolate.interp1d((timed_miap-tdats[0])/np.timedelta64(1,'s'), water_miap,kind='linear',bounds_error=False) 
f_gns = interpolate.interp1d((timed_miap-tdats[0])/np.timedelta64(1,'s'), water_gnss,kind='linear',bounds_error=False) 
 
rrp = f_rrp((tdats-tdats[0])/np.timedelta64(1,'s')) 
rrp = np.ma.array(rrp,mask=np.isnan(rrp)) 
gns = f_gns((tdats-tdats[0])/np.timedelta64(1,'s')) 
gns = np.ma.array(gns,mask=np.isnan(gns))
 
from sklearn.linear_model import LinearRegression, TheilSenRegressor 
from sklearn.linear_model import RANSACRegressor, HuberRegressor 

name = 'Huber' 
estimator = HuberRegressor(max_iter=10000) 

gns.mask = np.zeros(gns.shape)
gns[np.isnan(gns)] = 0
rrp[np.isnan(gns)] = 0
gns[gns==0]=np.ma.masked
rrp[rrp==0]=np.ma.masked 
 
X = data
print(X)
pca = PCA(n_components=8) 

ica = FastICA(n_components=8) 
fa = FactorAnalysis()
scores_dr = [] 
scores_regr = {} 
dimr = {"PCA:":pca,"FA":fa,"ICA":ica} 
for esn in dimr: 
    es = dimr[esn] 
    es.fit(X) 
    pc = es.transform(X) 
    for iPC in range(pc.shape[1]): 
        name = 'Huber' 
        estimator = HuberRegressor(max_iter=10000) 
        Xr = pc[:,iPC][np.logical_not(gns.mask),None] 
        yr = gns[np.logical_not(gns.mask)] 
        estimator.fit(Xr, yr) 
        scor = estimator.score(Xr, yr) 
        scores_regr[esn+"{}".format(iPC)]=scor 
        #print("Score:",scor," estimator:",esn,' C:',iPC)
pc = ica.transform(X)
np.savetxt('pc.txt', pc)


from sklearn.neighbors import KNeighborsRegressor 
from sklearn.model_selection import train_test_split 
from sklearn.neural_network import MLPRegressor  
from sklearn.model_selection import cross_val_score
from sklearn.metrics import mean_absolute_error ,mean_squared_error, median_absolute_error,confusion_matrix,accuracy_score,r2_score
import sklearn.metrics as metrics


knn_regr = KNeighborsRegressor(n_neighbors=3) 
regr = MLPRegressor(random_state=1, max_iter=5000) 
 
X = data[np.logical_not(gns.mask)] 
X = pc[np.logical_not(gns.mask)] 
y = gns[np.logical_not(gns.mask)] 
X_train, X_test, Y_train, Y_test = train_test_split(X, y, test_size=0.33, random_state=42) 
 
knn_regr.fit(X_train,Y_train) 
water_knn = knn_regr.predict(pc) 

y_pred_test = knn_regr.predict(X_test)
y_pred_train = knn_regr.predict(X_train)
 
plt.figure(figsize=(13,8))
plt.plot(timed_miap,water_miap,'b', label='radiometr',linewidth=2) 
plt.plot(timed_miap,water_gnss,'r', label='GNS',linewidth=2)
plt.plot(tdats,water_knn, label='KNN', alpha=0.5,linewidth=0.75) 
xlim = plt.xlim()
plt.plot(timed3,water3,label='MMERA-2 Model',linewidth=3)
plt.plot(timed4,water4,label='MODIS-Aqua',linewidth=3)
plt.xlabel('Time', size = 14)
plt.ylabel('PWV, cm', size = 14)
plt.ylim(-1,5)
plt.xlim(xlim)
plt.legend()
plt.tight_layout()
plt.savefig('Knn_miap.png')
plt.show()

print(' test mean squared error = ', np.sqrt(metrics.mean_squared_error(Y_test, y_pred_test)), '\n',
      'train mean squared error = ', np.sqrt(metrics.mean_squared_error(Y_train, y_pred_train)), '\n')

np.savetxt('water_KNN.dat',water_knn)
np.savetxt('gns.dat',gns.data)
np.savetxt('rrp.dat',rrp.data)
np.savetxt('gns_mask.dat',gns.mask)
np.savetxt('rrp_mask.dat',rrp.mask)
np.savetxt('ica.dat', pc)