from statsmodels.robust import mad as stand_mad
from scipy import stats
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import os

from scipy import interpolate

import time
from sklearn.linear_model import LinearRegression, TheilSenRegressor
from sklearn.linear_model import RANSACRegressor, HuberRegressor

from sklearn.preprocessing import StandardScaler, MinMaxScaler, FunctionTransformer
from sklearn.decomposition import PCA, KernelPCA, SparsePCA, FastICA, FactorAnalysis
from sklearn.decomposition import LatentDirichletAllocation
from sklearn.model_selection import cross_val_score, train_test_split
from sklearn.metrics import mean_absolute_error ,mean_squared_error, median_absolute_error,confusion_matrix,accuracy_score,r2_score
import sklearn.metrics as metrics

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers.experimental import preprocessing


if 'tdats' not in dir():
    tdats = []
    data = np.zeros((0,12))


def load(in_name = 'MIAP-2 data/01_Badary',i=0):
    import datetime
    
    uc = np.array([10,11,12,13,14,15,16,17,18,19,20,21])
    uc2 = uc - 2
        
    year = 2016 + i%12
    tdat = []
    i = 0
    for l in open(in_name+'.dat',encoding='CP1251').readlines():
        i+=1
        tt = l.split('\t')[6]
        td = tt.split(' ')        
        while '' in td: 
            td.remove('')
        try:
            tdat.append(datetime.datetime.strptime(tt,'%d.%m.%Y %H:%M:%S'))
        except:
            try:
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[2]),int(td[3]),int(td[4])))
            except:
                tdat.append(datetime.datetime(year,i,int(td[0]),int(td[1]),int(td[2]),int(td[3])))
    
            uc2=uc
    
            
    
    data2 = np.loadtxt(in_name+'.dat',usecols=uc2)
    return(tdat,data2)   
  
td,dd = load('bdr')
data = dd
tdats = np.array(td)
    


data=data.swapaxes(0,1)
data[:6] -= data[:6].mean(axis=0)
data[6:] -= data[6:].mean(axis=0)
data=data.swapaxes(0,1)
#X = scaler.fit_transform(data)

data_2016 = pd.read_csv('WVR & GNSS data/txt.Stemp.2016.11.09.13.14.42.txt',comment='#',keep_default_na=False)
data_2017 = pd.read_csv('WVR & GNSS data/txt.Stemp.2017.06.09.08.41.51.txt',comment='#',keep_default_na=False)

data_2016[data_2016 == data_2016['wt2'][2697]] = np.nan
data_2016[data_2016 == data_2016['wat1'][3630]] = np.nan
data_2017[data_2017 == data_2017['wt2'][3453]]=np.nan
data_2017[data_2017 == data_2017['wat1'][1460]]=np.nan

timed1 = pd.to_datetime(data_2016['date'] + ' ' + data_2016['time']).to_numpy(dtype=np.datetime64)
timed2 = pd.to_datetime(data_2017['date'] + ' ' + data_2017['time']).to_numpy(dtype=np.datetime64)

water_rad1 = data_2016['wt2'].to_numpy(dtype=float)
water_gnss1 = data_2016['wat1'].to_numpy(dtype=float)
water_rad2 = data_2017['wt2'].to_numpy(dtype=float)
water_gnss2 = data_2017['wat1'].to_numpy(dtype=float)

water_rad1[water_rad1 < 0] = 0
water_gnss1[water_gnss1 < 0] = 0
water_rad2[water_rad2 < 0] = 0
water_gnss2[water_gnss2 < 0] = 0

timed_miap = timed1[:13248]
timed_miap = np.append(timed1[:13248], timed2)
water_miap = water_rad1[:13248]
water_miap = np.append(water_rad1[:13248], water_rad2)
water_gnss = water_gnss1[:13248]
water_gnss = np.append(water_gnss1[:13248], water_gnss2)


def hampel(data, window_size, simg=3):    
    n = len(data)
    new_data = data.copy()
    k = 1.4826
    idx = []

    for i in range((window_size),(n - window_size)):
        r_median = np.median(data[(i - window_size):(i + window_size)]) 
        r_mad  = k * np.median(np.abs(data[(i - window_size):(i + window_size)] - r_median)) 
        
        if (np.abs(data[i] - r_median) > simg * r_mad):
            new_data[i] = r_median #замена выброса
            idx.append(i)

    return new_data, idx

water_miap, outliers = hampel(water_miap, 15) # 5 hours
water_gnss, outliers = hampel(water_gnss, 15)
 
tdats = np.array(tdats,dtype=np.datetime64) 

from scipy import interpolate 

f_rrp = interpolate.interp1d((timed_miap-tdats[0])/np.timedelta64(1,'s'), water_miap,kind='linear',bounds_error=False) 
f_gns = interpolate.interp1d((timed_miap-tdats[0])/np.timedelta64(1,'s'), water_gnss,kind='linear',bounds_error=False) 
 
rrp = f_rrp((tdats-tdats[0])/np.timedelta64(1,'s')) 
rrp = np.ma.array(rrp,mask=np.isnan(rrp)) 
gns = f_gns((tdats-tdats[0])/np.timedelta64(1,'s')) 
gns = np.ma.array(gns,mask=np.isnan(gns))



name = 'Huber' 
estimator = HuberRegressor(max_iter=10000) 

gns.mask = np.zeros(gns.shape)
gns[np.isnan(gns)] = 0
rrp[np.isnan(gns)] = 0
gns[gns==0]=np.ma.masked
rrp[rrp==0]=np.ma.masked 

X = data

pca = PCA(n_components=8)
ica = FastICA(n_components=8)
fa = FactorAnalysis()
scores_dr = []
scores_regr = {}
dimr = {"PCA:":pca,"FA":fa,"ICA":ica}
for esn in dimr:
    es = dimr[esn]
    es.fit(X)
    pc = es.transform(X)
    for iPC in range(pc.shape[1]):
        name = 'Huber'
        estimator = HuberRegressor(max_iter=10000)
        Xr = pc[:,iPC][np.logical_not(gns.mask),None]
        yr = gns[np.logical_not(gns.mask)]
        estimator.fit(Xr, yr)
        scor = estimator.score(Xr, yr)
        scores_regr[esn+"{}".format(iPC)]=scor
        print("Score:",scor," estimator:",esn,' C:',iPC)

pc = ica.transform(X)

tf.keras.backend.clear_session()


s = 2
X = pc[np.logical_not(gns.mask)]
y = gns[np.logical_not(gns.mask)][2:-2]
t = tdats[np.logical_not(gns.mask)][2:-2]

yscaler = MinMaxScaler((0,1))
xscaler = MinMaxScaler((-1,1))
y_transform = yscaler.fit_transform(y[:, None])[:,0]
X_transform = xscaler.fit_transform(X)

X_ = np.zeros((X.shape[0]-4, 5, X.shape[1]))
for i in range(5):
    X_[:,i] = X_transform[i:X.shape[0]-(4)+i]


X_train, X_test, y_train, y_test = train_test_split(X_, y_transform, test_size=0.25)
ss = np.array(X_.shape[1:])

l = 4
p = np.zeros(l)
models = np.zeros(l, dtype=object)

for i in range(l):
    model = keras.Sequential([
          layers.Flatten(input_shape=(ss)),
          layers.Dense((i+1)*ss.prod()**2, activation='relu',
                       kernel_initializer='random_uniform',
                       bias_initializer='zeros'),
          layers.Dense((i+1)*ss[1]**2, activation='tanh',
                       kernel_initializer='random_uniform',
                       bias_initializer='zeros'),
          layers.Dense(1)])

    model.compile(optimizer='adam',
        loss='mean_absolute_error')

    model.summary()
    
    logdir=f"logs/fit/{i}/"
    tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
    model.fit(X_train, y_train, epochs=64, callbacks=[tensorboard_callback])

    predicted = model.predict(X_test)[:,0]
    y_pred_test = model.predict(X_test)[:,0]
    y_pred_train = model.predict(X_train)[:,0]
    
    plt.figure(figsize=(13,8))
    plt.scatter(yscaler.inverse_transform(y_test[:, None])[:,0], yscaler.inverse_transform(predicted[:, None])[:,0], s = 1, marker='x',zorder=3,alpha=0.25)
    plt.xlabel('GNSS, g/cm$^2$')
    plt.ylabel('Предсказание, g/cm$^2$')
    plt.xlim(0,2)
    plt.ylim(0,2)
    plt.show()
    
    plt.figure(figsize=(13,8))
    water_ann = model.predict(X_)
    plt.plot(t,y,'r', label='GNS',linewidth=2)
    plt.plot(t,yscaler.inverse_transform(water_ann), label='KNN', alpha=0.5,linewidth=0.75)
    plt.legend()
    plt.show()


t = tdats[np.logical_not(gns.mask)][2:-2]
plt.figure(figsize=(13,8))
plt.plot(t,y,'r', label='GNS',linewidth=2)
plt.plot(t,yscaler.inverse_transform(water_ann), label='KNN', alpha=0.5,linewidth=0.75)
plt.legend()

'''
,
          layers.Dense((i+1)*ss.prod()**2, 
                       kernel_initializer='random_uniform',
                       bias_initializer='zeros', activation='linear'),
          layers.Dense((i+1)*ss[1]**2, 
                       kernel_initializer='random_uniform',
                       bias_initializer='zeros', activation='exponential'),
          layers.Dense((i+1)*ss[1]**2, 
                       kernel_initializer='random_uniform',
                       bias_initializer='zeros', activation='linear')
'''