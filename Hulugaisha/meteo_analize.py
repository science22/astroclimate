#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 11:36:40 2024

@author: edombek
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statsmodels.tsa.stattools import ccf
from numpy import ma

def str2np(s):
	return np.array(s.split(), dtype=float)
    
def getParOnZ(df, Z, c):
    ret = np.zeros(len(time))
    for i, t in enumerate(time):
        x = df.loc[t]
        try:
            ret[i] = np.interp(Z,x['z'],x[c])
        except:
            ret[i] = np.nan
    return ret

igra = pd.read_csv('igra_weather.csv', index_col='date', parse_dates=True)
wg = pd.read_csv('wg.csv', index_col='date', parse_dates=True)
time = igra.index

for t in time:
    for c in igra.columns:
        if type(igra[c][t]) is str:
            igra.loc[t,c] = str2np(igra[c][t][1:-1])
        try:
            if type(wg[c][t]) is str:
                wg.loc[t,c] = str2np(wg[c][t][1:-1])
        except:
            wg.loc[t,c] = np.nan

tigra = ma.masked_invalid(getParOnZ(igra,2.7,'t'))
twg = ma.masked_invalid(getParOnZ(wg,2.7,'t'))


fig, ax = plt.subplots(figsize=(8,8))
ax.scatter(tigra, twg, s=1)
ax.set_xlabel('$T_{igra}, K$')
ax.set_ylabel('$T_{wg}, K$')
ax.set_xlim(230, 310)
ax.set_ylim(230, 310)
fig.tight_layout()

def correlate(x,y, N=100):
    ret = np.zeros(N)
    x -= x.mean()
    y -= y.mean()
    x /= np.abs(x).max()
    y /= np.abs(y).max()
    for i in range(N):
        ret[i] = np.mean(x[i:]*y[i:])
    return ret

N = 365*4
t = np.arange(N)/2
corr = correlate(tigra.copy(), twg.copy(), N)
fig, ax = plt.subplots(figsize=(12,6))
ax.plot(t, corr)
ax.set_xlabel(r't, day')
ax.set_ylabel('corr($T_{igra}, T_{wg}$)')
fig.tight_layout()

corr = correlate(tigra.copy(), tigra.copy(), N)
fig, ax = plt.subplots(figsize=(12,6))
ax.plot(t, corr)
ax.set_xlabel(r't, day')
ax.set_ylabel('autocorr($T_{igra}$)')
fig.tight_layout()

rhigra = ma.masked_invalid(getParOnZ(igra,2.7,'rh'))
rhwg = ma.masked_invalid(getParOnZ(wg,2.7,'rh'))
rhwg.mask[rhwg > 1] = True

corr = correlate(rhigra.copy(), rhwg.copy(), N)
fig, ax = plt.subplots(figsize=(12,6))
ax.plot(t, corr)
ax.set_xlabel(r't, day')
ax.set_ylabel('corr($RH_{igra}, RH_{wg}$)')
fig.tight_layout()

corr = correlate(rhigra.copy(), rhigra.copy(), N)
fig, ax = plt.subplots(figsize=(12,6))
ax.plot(t, corr)
ax.set_xlabel(r't, day')
ax.set_ylabel('autocorr($RH_{igra}$)')
fig.tight_layout()

fig, ax = plt.subplots(figsize=(8,8))
ax.scatter(rhigra, rhwg, s=1)
ax.set_xlabel('$RH_{igra}$')
ax.set_ylabel('$RH_{wg}$')
ax.set_xlim(0, 1)
ax.set_ylim(0, 1)
fig.tight_layout()